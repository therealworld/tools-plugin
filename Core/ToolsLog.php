<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;

class ToolsLog
{
    /** Log-Array */
    protected static array $_aLog = [];

    /** the Debug Log */
    protected static array $_aDebugLog = [];

    /** possible Log Levels */
    protected static array $_aPossibleLogLevels = [
        'emergency', 'alert', 'critical', 'error', 'warning', 'info', 'notice', 'debug',
    ];

    /** the default Log Level */
    protected static string $_sDefaultLogLevel = 'info';

    /**
     * set Log entries.
     *
     * @param string $sMessage  - Log Message -> StandardMessages would be translate
     * @param string $sChannel  - Log Channel
     * @param string $sLogLevel - Log Level type
     */
    public static function setLogEntry(string $sMessage = '', string $sChannel = '', string $sLogLevel = ''): void
    {
        if ($sMessage) {
            $sMessage = (string) Registry::getLang()->translateString($sMessage);

            $_aTmp = [
                'channel' => $sChannel,
                'level'   => in_array($sLogLevel, self::getPossibleLogLevels(), true) ?
                    $sLogLevel :
                    self::$_sDefaultLogLevel,
                'message'  => $sMessage,
                'datetime' => self::getMicroTime(),
            ];
            self::$_aLog[] = $_aTmp;
        }
    }

    /**
     * set many Log entries.
     *
     * @param array  $aOxMessages - Array with Log Messages
     * @param string $sChannel    - Log Channel
     * @param string $sLogLevel   - Log Level type
     */
    public static function setLogEntries(array $aOxMessages = [], string $sChannel = '', string $sLogLevel = ''): void
    {
        foreach ($aOxMessages as $sMessage) {
            self::setLogEntry($sMessage, $sChannel, $sLogLevel);
        }
    }

    /** get Log entries */
    public static function getLog(): array
    {
        return self::$_aLog;
    }

    /** set Log entries */
    public static function setLog(array $aLog = []): void
    {
        self::$_aLog = $aLog;
    }

    /** get possible LogLevels */
    public static function getPossibleLogLevels(): array
    {
        return self::$_aPossibleLogLevels;
    }

    /**
     * get Log entries.
     *
     * @param string $sDelimiter - String delimiter for the Log-Entries
     */
    public static function getLogAsText(string $sDelimiter = "\n"): string
    {
        $sResult = '';
        foreach (self::$_aLog as $aLogItem) {
            $sResult .= self::getDateFromMicroTime($aLogItem['datetime']) . ' ' . $aLogItem['message'] . $sDelimiter;
        }

        return $sResult;
    }

    /** is Log entries */
    public static function isLog(): bool
    {
        return (bool) count(self::$_aLog);
    }

    /** reset Log entries */
    public static function resetLogEntry(): void
    {
        self::$_aLog = [];
    }

    /**
     * set a debug Log Entry.
     *
     * @param mixed $mLogEntry - Log entry (string, array, obj ...)
     */
    public static function setDebugLogEntry($mLogEntry): void
    {
        self::$_aDebugLog[] = $mLogEntry;
    }

    /**
     * get the debug Log.
     *
     * @param bool $bTargetTRWToolsLog - should send the Log to the TRWTools
     */
    public static function getDebugLog(bool $bTargetTRWToolsLog = false, string $sChannel = ''): array
    {
        $aResult = [];

        if (count(self::$_aDebugLog)) {
            foreach (self::$_aDebugLog as $mLogEntryKey => $mLogEntryValue) {
                // convert object and array to string
                if (is_object($mLogEntryValue) || is_array($mLogEntryValue)) {
                    $aResult[$mLogEntryKey] = $bTargetTRWToolsLog ?
                        print_r($mLogEntryValue, true) :
                        json_encode($mLogEntryValue, JSON_THROW_ON_ERROR);
                } else {
                    $aResult[$mLogEntryKey] = $mLogEntryKey . ' : ' . $mLogEntryValue;
                }
            }
        }

        if ($bTargetTRWToolsLog) {
            self::setLogEntries($aResult, $sChannel);
        }

        return $aResult;
    }

    protected static function getMicroTime(): string
    {
        $fMicroTime = microtime(true);
        $sMicroSeconds = sprintf('%06d', ($fMicroTime - floor($fMicroTime)) * 1000000);
        $fSeconds = floor($fMicroTime);
        return $fSeconds . '.' . $sMicroSeconds;
    }

    protected static function getDateFromMicroTime(string $sMicroTimeStamp = ''): string
    {
        $fMicroTime = $sMicroTimeStamp ? (float)$sMicroTimeStamp : microtime(true);
        $sMicroSeconds = sprintf('%06d', ($fMicroTime - floor($fMicroTime)) * 1000000);
        $fSeconds = (int)floor($fMicroTime);
        return date('Y:m:d H:i:s', $fSeconds) . '.' . $sMicroSeconds;
    }
}
