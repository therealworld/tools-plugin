<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use OxidEsales\Eshop\Application\Model\Category;
use OxidEsales\Eshop\Application\Model\Manufacturer;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Model\MultiLanguageModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\TableViewNameGenerator;

class ToolsOxid
{
    /**
     * Returns string built from category titles.
     *
     * @param Category $oCat category object
     * @param string   $sSep in Category Path
     */
    public static function getCatPath(Category $oCat, string $sSep = ' / '): string
    {
        $sCatPathString = '';
        $sTmpSep = '';
        while ($oCat) {
            // prepare oCat title part
            $sCatPathString = $oCat->getFieldData('oxtitle') . $sTmpSep . $sCatPathString;
            $sTmpSep = $sSep;
            // load parent
            $oCat = $oCat->getParentCategory();
        }

        return $sCatPathString;
    }

    /**
     * set a Attribute Value.
     *
     * @param string $sOxObjectId    the oxid of article
     * @param string $sAttributeOxId the oxid of attribute
     * @param string $sValue         the title
     *
     * @throws Exception
     */
    public static function setAttributeValue(
        string $sOxObjectId = '',
        string $sAttributeOxId = '',
        string $sValue = '',
        int $iLangId = 0
    ): void {
        if ($sAttributeOxId && $sValue) {
            $oObj2Attrib = oxNew(MultiLanguageModel::class);
            $oObj2Attrib->init('oxobject2attribute');
            $oObj2Attrib->setLanguage($iLangId);

            $sAttribValueOxId = ToolsDB::getAnyId(
                'oxobject2attribute',
                [
                    'oxobjectid' => $sOxObjectId,
                    'oxattrid'   => $sAttributeOxId,
                ]
            );

            if (!$sAttribValueOxId) {
                $sAttribValueOxId = Registry::getUtilsObject()->generateUID();
            }
            $oObj2Attrib->loadInLang($iLangId, $sAttribValueOxId);
            $oObj2Attrib->setId($sAttribValueOxId);
            $aParams = [
                'oxobjectid' => $sOxObjectId,
                'oxattrid'   => $sAttributeOxId,
                'oxvalue'    => $sValue,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxobject2attribute');
            $oObj2Attrib->assign($aParams);
            $oObj2Attrib->save();
        }
    }

    /**
     * Save Manufacturer.
     *
     * @throws Exception
     */
    public static function setManufacturer(string $sTitle = '', string $sDescription = '', int $iLangId = 0): string
    {
        $sOxId = '';
        if ($sTitle) {
            $oManufacturer = oxNew(Manufacturer::class);
            $oManufacturer->setLanguage($iLangId);

            if ($sOxId = ToolsDB::getAnyId('oxmanufacturers', ['oxtitle' => $sTitle], 'oxid', $iLangId)) {
                $oManufacturer->load($sOxId);
            }

            $aParams = [
                'oxtitle'     => $sTitle,
                'oxshortdesc' => $sDescription,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxmanufacturers');

            $oManufacturer->assign($aParams);
            $oManufacturer->save();
            $sOxId = $oManufacturer->getId();

            foreach (Registry::getLang()->getAllShopLanguageIds() as $iLang => $sLang) {
                $oManufacturer->loadInLang($iLangId, $sOxId);
                $oManufacturer->assign($aParams);
                $oManufacturer->save();
            }
        }

        return $sOxId;
    }

    /**
     * Save link between Article and category.
     *
     * @param string $sOxCatnId
     *
     * @throws Exception
     */
    public static function setArticleToCategory(string $sOxObjectId = '', $sOxCatnId = ''): string
    {
        $sOxId = '';

        if ($sOxObjectId && $sOxCatnId) {
            $sOxId = ToolsDB::getAnyId('oxobject2category', ['oxobjectid' => $sOxObjectId, 'oxcatnid' => $sOxCatnId]);
            $oOxObject2Category = oxNew(BaseModel::class);
            $oOxObject2Category->init('oxobject2category');

            if ($sOxId) {
                $oOxObject2Category->load($sOxId);
            }

            $aParams = [
                'oxobjectid' => $sOxObjectId,
                'oxcatnid'   => $sOxCatnId,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxobject2category');
            $oOxObject2Category->assign($aParams);
            $oOxObject2Category->save();
            $sOxId = $oOxObject2Category->getId();
            unset($oOxObject2Category);
        }

        return $sOxId;
    }

    /**
     * set a CategoryTitle.
     *
     * @param string $sTitle        the title
     * @param string $sCategoriesId the possible Id of the category
     * @param string $sParentId     the Parent OxId
     * @param string $sRootOxCatId  the Root OxId
     *
     * @throws DatabaseConnectionException
     * @throws Exception
     */
    public static function setCategoryTitle(
        string $sTitle = '',
        string $sCategoriesId = '',
        string $sParentId = '',
        string $sRootOxCatId = '',
        int $iLangId = 0
    ): string {
        $sResult = '';
        if ($sTitle) {
            if (!$sCategoriesId) {
                $oDb = DatabaseProvider::getDb();
                $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
                $sCatViewName = $viewNameGenerator->getViewName('oxcategories', $iLangId);
                $sSelect = "select `oxid`
                    from {$sCatViewName}
                    where lower(`oxtitle`) = " . $oDb->quote(Str::getStr()->strtolower($sTitle));
                $sResult = $oDb->getOne($sSelect);
            }

            if (!$sResult) {
                $oCategory = oxNew(Category::class);
                $oCategory->setLanguage($iLangId);

                // if it is new, we set some additional informations
                $aParams = [
                    'oxactive'   => 1,
                    'oxtitle'    => $sTitle,
                    'oxparentid' => $sParentId,
                    'oxrootid'   => $sRootOxCatId,
                ];
                $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcategories');
                $oCategory->assign($aParams);
                $oCategory->save();
                $sResult = $oCategory->getId();
            }
        }

        return $sResult;
    }
}
