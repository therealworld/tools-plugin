<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Exception\DatabaseException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Theme;

class ToolsTheme
{
    /** Config Keys that must be inside of a ConfigTransfer-Array */
    protected static array $_aConfigTransferKeys = [
        'type', 'value', 'group', 'constraints', 'pos',
    ];

    /**
     * Get the List of themes.
     *
     * @param null|int $iShopId - ShopId
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws DatabaseException
     */
    public static function getAllThemeOptions(array $aExcludeConfigFields = [], ?int $iShopId = null): array
    {
        $oConfig = Registry::getConfig();

        $iShopId ??= $oConfig->getShopId();

        $oConfig->initVars($iShopId);

        if (!$sThemeForShopId = $oConfig->getConfigParam('sCustomTheme')) {
            $sThemeForShopId = $oConfig->getConfigParam('sTheme');
        }

        $oTheme = oxNew(Theme::class);
        $aAllThemeOptions = [];

        foreach ($oTheme->getList() as $oThemeElement) {
            $sThemeId = $oThemeElement->getInfo('id');
            $aThemeOptions = [
                'id'          => $sThemeId,
                'title'       => $oThemeElement->getInfo('title'),
                'description' => $oThemeElement->getInfo('description'),
                'thumbnail'   => $oThemeElement->getInfo('thumbnail'),
                'version'     => $oThemeElement->getInfo('version'),
                'author'      => $oThemeElement->getInfo('author'),
                'active'      => $sThemeForShopId === $sThemeId,
            ];
            if ($oThemeElement->getInfo('parentTheme')) {
                $aThemeOptions['parenttheme'] = $oThemeElement->getInfo('parentTheme');
                $aThemeOptions['parentversions'] = $oThemeElement->getInfo('parentVersions');
            }

            $aThemeSettings = ToolsConfig::getShopConfigValuesForExport([], $iShopId, 'theme:' . $sThemeId);

            foreach ($aThemeSettings as $sSettingsName => $aSetting) {
                // dont export this option
                if (in_array($sSettingsName, $aExcludeConfigFields, true)) {
                    continue;
                }
                $aThemeOptions['themeSettings'][$sSettingsName] = $aSetting;
                if (empty($aSetting['constraints'])) {
                    unset($aThemeOptions['themeSettings'][$sSettingsName]['constraints']);
                } elseif (!is_array($aSetting['constraints'])) {
                    $aThemeOptions['themeSettings'][$sSettingsName]['constraints'] = explode(
                        '|',
                        $aSetting['constraints']
                    );
                }
            }
            $aAllThemeOptions[$sThemeId] = $aThemeOptions;
        }

        return $aAllThemeOptions;
    }

    /**
     * Get the Themeoptions as Array.
     *
     * @param null|int $iShopId - ShopId
     *
     * @throws DatabaseException
     */
    public static function getThemeOptions(string $sThemeId = '', ?int $iShopId = null): array
    {
        $aResult = [];

        $oConfig = Registry::getConfig();

        $iShopId ??= $oConfig->getShopId();

        $oConfig->initVars($iShopId);

        return $sThemeId ? ToolsConfig::getShopConfigValuesForExport([], $iShopId, 'theme:' . $sThemeId) : [];
    }

    /**
     * Set Themeoptions for a target theme.
     *
     * @param string $sThemeId      - Id of the Theme
     * @param array  $aThemeOptions - Array with ThemeOptions
     */
    public static function setThemeOptions(string $sThemeId = '', array $aThemeOptions = []): void
    {
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $oConfig = Registry::getConfig();
        $sShopId = $oConfig->getBaseShopId();

        $oTheme = oxNew(Theme::class);
        $oTheme->load($sThemeId);

        if (is_array($aThemeOptions) && count($aThemeOptions) && $oTheme->getId()) {
            foreach ($aThemeOptions as $sConfigName => $aConfig) {
                // check if ConfigTransfer-Array are complete
                foreach (self::$_aConfigTransferKeys as $sConfigKey) {
                    if (!array_key_exists($sConfigKey, $aConfig)) {
                        $aConfig[$sConfigKey] = null;
                    }
                }

                // check for array-Variables
                if (is_array($aConfig['value']) && in_array($aConfig['type'], ['arr', 'aarr'])) {
                    $aConfig['value'] = serialize($aConfig['value']);
                }

                // check if option exists
                if (
                    !ToolsDB::getAnyId(
                        'oxconfig',
                        [
                            'oxvarname' => $sConfigName,
                            'oxmodule'  => 'theme:' . $sThemeId,
                        ]
                    )
                ) {
                    $sOxId = md5($sThemeId . '.' . $sConfigName);

                    $sSql = 'replace into `oxconfig`
                        set `oxid` = ' . $oDb->quote($sOxId) . ',
                            `oxshopid` = ' . $oDb->quote($sShopId) . ',
                            `oxmodule` = ' . $oDb->quote('theme:' . $sThemeId) . ',
                            `oxvarname` = ' . $oDb->quote($sConfigName) . ',
                            `oxvartype` = ' . $oDb->quote($aConfig['type']) . ',
                            `oxvarvalue` = encode( ' . $oDb->quote($aConfig['value']) . ', ' .
                        $oDb->quote($oConfig->getConfigParam('sConfigKey')) . ')';
                    ToolsDB::execute($sSql);

                    // display
                    $sSql = 'replace into `oxconfigdisplay`
                        set `oxid` = ' . $oDb->quote($sOxId) . ',
                            `oxcfgmodule` = ' . $oDb->quote('theme:' . $sThemeId) . ',
                            `oxcfgvarname` = ' . $oDb->quote($sConfigName) . ',
                            `oxgrouping` = ' . $oDb->quote($aConfig['group']) . ',
                            `oxvarconstraint` = ' . $oDb->quote($aConfig['constraints']) . ',
                            `oxpos` = ' . $oDb->quote($aConfig['pos']);
                    ToolsDB::execute($sSql);
                }
            }
        }
    }

    /**
     * delete all or a special theme option.
     *
     * @param string $sThemeId      - Id of the Theme
     * @param array  $aThemeOptions - Array with ThemeOptions (if empty all options would be delete)
     */
    public static function deleteThemeOptions(string $sThemeId = '', array $aThemeOptions = []): void
    {
        $oTheme = oxNew(Theme::class);
        $oTheme->load($sThemeId);

        if ($oTheme->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sShopId = Registry::getConfig()->getBaseShopId();

            // delete existing values for theme
            if (is_array($aThemeOptions)) {
                foreach ($aThemeOptions as $sOptionName) {
                    $sSql = 'delete from `oxconfig`
                        where `oxshopid` = ' . $oDb->quote($sShopId) . '
                        and `oxmodule` = ' . $oDb->quote('theme:' . $sThemeId) . '
                        and `oxvarname` = ' . $oDb->quote($sOptionName);
                    ToolsDB::execute($sSql);

                    $sSql = 'delete from `oxconfigdisplay`
                        where `oxcfgmodule` = ' . $oDb->quote('theme:' . $sThemeId) . '
                        and `oxcfgvarname` = ' . $oDb->quote($sOptionName);
                    ToolsDB::execute($sSql);
                }
            } else {
                // delete all theme options
                $sSql = 'delete from `oxconfig`
                    where `oxshopid` = ' . $oDb->quote($sShopId) . '
                    and `oxmodule` = ' . $oDb->quote('theme:' . $sThemeId);
                ToolsDB::execute($sSql);

                $sSql = 'delete from `oxconfigdisplay`
                    where `oxcfgmodule` = ' . $oDb->quote('theme:' . $sThemeId);
                ToolsDB::execute($sSql);
            }
        }
    }

    /**
     * get Option Groups from theme.
     *
     * @param string $sThemeId - Id of the Theme
     *
     * @return mixed (boolean / array)
     *
     * @throws DatabaseErrorException
     */
    public static function getOptionGroups(string $sThemeId = ''): array
    {
        $aResult = [];
        $oTheme = oxNew(Theme::class);
        $oTheme->load($sThemeId);

        if ($oTheme->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSelect = 'select `oxgrouping`
                from `oxconfigdisplay`
                where `oxcfgmodule` = ' . $oDb->quote('theme:' . $sThemeId) . '
                group by `oxgrouping`
                order by `oxgrouping`';

            $oResults = $oDb->select($sSelect);
            if ($oResults->count() > 0) {
                while (!$oResults->EOF) {
                    $aRow = $oResults->getFields();
                    $aResult[] = $aRow;
                    $oResults->fetchRow();
                }
            }
        }

        return $aResult;
    }
}
