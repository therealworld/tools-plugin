<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Model\ListModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\TableViewNameGenerator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ToolsDB
{
    /**
     * get OXID id or others from given table.
     *
     * @param string       $sTable            - The table where i found the sObject
     * @param array        $aWhere            - an array for and associated conditions
     * @param array|string $mObject           - search for a special column (string) or many columns (array)
     * @param int          $iLangId           - search in a special language?
     * @param bool         $bColognePhonetics - search with Cologne Phonetics
     *
     * @return array|bool|string - normally string
     */
    public static function getAnyId(
        string $sTable,
        array $aWhere = [],
        $mObject = 'oxid',
        int $iLangId = 0,
        bool $bColognePhonetics = false
    ) {
        $mResult = false;
        if ($sTable && is_array($aWhere)) {
            $oDb = DatabaseProvider::getDb();

            $viewNameGenerator = Registry::get(TableViewNameGenerator::class);

            $sSearch = is_array($mObject) ? implode(', ', $mObject) : $mObject;

            $sSelect = 'select ' . $sSearch . '
                from ' . $viewNameGenerator->getViewName($sTable, $iLangId) . '
                where 1 ';

            if (count($aWhere)) {
                foreach ($aWhere as $sName => $sValue) {
                    if ($bColognePhonetics) {
                        $sSelect .= ' and ( fnc_cologne_match(' . $oDb->quote($sValue) . ', ' .
                            $sName . ", ' ') = 1 ) ";
                    } else {
                        $sSelect .= ' and ' . $sName . ' = ' . $oDb->quote($sValue);
                    }
                }
            }
            $sSelect .= ' limit 1';

            if (is_array($mObject)) {
                $oList = oxNew(ListModel::class);
                $oList->selectString($sSelect);
                if ($oList->count()) {
                    $mResult = [];
                    $oRow = $oList->current();
                    foreach ($mObject as $sKey) {
                        $mResult[$sKey] = $oRow->{$sTable . '__' . $sKey}->value;
                    }
                }
            } else {
                $mResult = $oDb->getOne($sSelect);
            }
        }

        return $mResult;
    }

    /**
     * convertDB2OxParams.
     *
     * convert a Oxid DB-Array to a typical Oxid Params-Array
     */
    public static function convertDB2OxParams(array $aData = [], string $sTable = ''): array
    {
        $aDataResult = [];
        if (is_array($aData) && $sTable) {
            foreach ($aData as $sKey => $mValue) {
                $sNewKey = Str::getStr()->strtolower($sTable . '__' . $sKey);
                if (is_float($mValue)) {
                    $mValue = str_replace(',', '.', (string) $mValue);
                }
                $aDataResult[$sNewKey] = $mValue;
            }
        }

        return $aDataResult;
    }

    /**
     * Check if table or table column exists.
     *
     * @param string $sTable  - Name of table
     * @param string $sColumn - Name of Column
     *
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    public static function tableColumnExists(string $sTable = '', string $sColumn = ''): bool
    {
        $bResult = false;
        if ($sTable) {
            $oDb = DatabaseProvider::getDb();

            if ($sColumn) {
                $sSql = "show columns from {$sTable} like " . $oDb->quote($sColumn);
            } else {
                $sSql = 'show tables like ' . $oDb->quote($sTable);
            }

            $oResults = $oDb->select($sSql);
            if ($oResults->count() > 0) {
                $bResult = true;
            }
        }

        return $bResult;
    }

    /**
     * Check if index exists.
     *
     * @param string $sTable - Name of table
     * @param string $sIndex - Name of index
     *
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    public static function tableIndexExists(string $sTable = '', string $sIndex = ''): bool
    {
        $bResult = false;
        if ($sTable && $sIndex) {
            $oDb = DatabaseProvider::getDb();

            $sSql = "show index from {$sTable} where key_name like " . $oDb->quote($sIndex);

            $oResults = $oDb->select($sSql);
            if ($oResults->count() > 0) {
                $bResult = true;
            }
        }

        return $bResult;
    }

    /**
     * set a table as Multilangual and Multishop.
     *
     * @param string $sTable - Name of table
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function setTableMulti(string $sTable = ''): void
    {
        if ($sTable) {
            self::setTableMultiLang($sTable);
            self::setTableMultiShop($sTable);
        }
    }

    /**
     * delete a table as Multilangual and Multishop.
     *
     * @param string $sTable - Name of table
     */
    public static function deleteTableMulti(string $sTable = ''): void
    {
        if ($sTable) {
            self::deleteTableMultiLang($sTable);
            self::deleteTableMultiShop($sTable);
        }
    }

    /**
     * set a table as Multilangual.
     *
     * @param string $sTable - Name of table
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function setTableMultiLang(string $sTable = ''): void
    {
        if ($sTable) {
            $oConfig = Registry::getConfig();

            foreach ($oConfig->getShopIds() as $iShopId) {
                $aMultiLangTables = $oConfig->getShopConfVar('aMultiLangTables', $iShopId);
                if (!is_array($aMultiLangTables)) {
                    $aMultiLangTables = Registry::getLang()->getMultiLangTables();
                }
                if (!in_array($sTable, $aMultiLangTables, true)) {
                    $aMultiLangTables[] = $sTable;
                }
                ToolsConfig::saveConfigParam('aMultiLangTables', $aMultiLangTables, 'arr', '', (int) $iShopId);
            }
        }
    }

    /**
     * delete a table as Multilangual.
     *
     * @param string $sTable - Name of table
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function deleteTableMultiLang(string $sTable = ''): void
    {
        if ($sTable) {
            $oConfig = Registry::getConfig();

            foreach ($oConfig->getShopIds() as $iShopId) {
                $aMultiLangTables = $oConfig->getShopConfVar('aMultiLangTables', $iShopId);
                if (!is_array($aMultiLangTables)) {
                    $aMultiLangTables = Registry::getLang()->getMultiLangTables();
                }
                if (in_array($sTable, $aMultiLangTables, true)) {
                    $aMultiLangTables = array_diff($aMultiLangTables, [$sTable]);
                    ToolsConfig::saveConfigParam('aMultiLangTables', $aMultiLangTables, 'arr', '', (int) $iShopId);
                }
            }
        }
    }

    /**
     * set a table as Multishop.
     *
     * @param string $sTable - Name of table
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function setTableMultiShop(string $sTable = ''): void
    {
        if ($sTable && ToolsConfig::isMultiShop()) {
            $oConfig = Registry::getConfig();

            foreach ($oConfig->getShopIds() as $iShopId) {
                // manipulate Option "aMultiShopTables"
                $aMultiShopTables = $oConfig->getShopConfVar('aMultiShopTables', $iShopId);

                if (!is_array($aMultiShopTables)) {
                    $aMultiShopTables = $oConfig->getConfigParam('aMultiShopTables');
                }

                if (!in_array($sTable, $aMultiShopTables, true)) {
                    $aMultiShopTables[] = $sTable;
                }
                ToolsConfig::saveConfigParam(
                    'aMultiShopTables',
                    $aMultiShopTables,
                    'arr',
                    '',
                    (int) $iShopId
                );

                // create blMallInherit_??? Variable
                $sMallInheritVar = 'blMallInherit_' . $sTable;
                $bMallInherit = $oConfig->getShopConfVar($sMallInheritVar, $iShopId);

                ToolsConfig::saveConfigParam(
                    $sMallInheritVar,
                    $bMallInherit,
                    'bool',
                    '',
                    (int) $iShopId
                );
            }
        }
    }

    /**
     * delete a table as Multishop.
     *
     * @param string $sTable - Name of table
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function deleteTableMultiShop(string $sTable = ''): void
    {
        if ($sTable && ToolsConfig::isMultiShop()) {
            $oConfig = Registry::getConfig();
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

            foreach ($oConfig->getShopIds() as $iShopId) {
                // manipulate Option "aMultiShopTables"
                $aMultiShopTables = $oConfig->getShopConfVar('aMultiShopTables', $iShopId);

                if (!is_array($aMultiShopTables)) {
                    $aMultiShopTables = $oConfig->getConfigParam('aMultiShopTables');
                }

                if (in_array($sTable, $aMultiShopTables, true)) {
                    $aMultiShopTables = array_diff($aMultiShopTables, [$sTable]);
                    ToolsConfig::saveConfigParam(
                        'aMultiShopTables',
                        $aMultiShopTables,
                        'arr',
                        '',
                        (int) $iShopId
                    );
                }

                // delete blMallInherit_??? Variable
                $sMallInheritVar = 'blMallInherit_' . $sTable;

                $sSql = "delete from `oxconfig`
                    where `oxmodule` = ''
                    and `oxvarname` = " . $oDb->quote($sMallInheritVar);
                self::execute($sSql);
            }
        }
    }

    /**
     * get a Array with Columns.
     *
     * @param string $sTableName  = Name of the Table
     * @param string $sColumnName = Name of the TableColumn
     */
    public static function showTableColumn(string $sTableName = '', string $sColumnName = ''): array
    {
        $aResult = [];

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $sSelect = "show columns
            from {$sTableName} " .
            ($sColumnName ? ' like ' . $oDb->quote($sColumnName) : '');

        $oResults = $oDb->select($sSelect);
        if ($oResults->count() > 0) {
            while (!$oResults->EOF) {
                $aRow = $oResults->getFields();
                $aResult[] = $aRow;
                $oResults->fetchRow();
            }
        }

        return $aResult;
    }

    /**
     * get a Array with Enum Values of a Field.
     *
     * @param string $sTableName  = Name of the Table
     * @param string $sColumnName = Name of the TableColumn
     *
     * @throws DatabaseErrorException
     */
    public static function showEnumValues(string $sTableName = '', string $sColumnName = ''): array
    {
        $aResult = [];
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $sSelect = "show columns from `{$sTableName}` like " . $oDb->quote($sColumnName);

        $oResults = $oDb->select($sSelect);
        if ($oResults->count() > 0) {
            $aRow = $oResults->getFields();
            if (preg_match('/enum\((.*)\)$/', $aRow['Type'], $aMatches)) {
                $aResult = str_getcsv($aMatches[1], ',', "'");
            }
        }

        return $aResult;
    }

    /**
     * delete Columns from table.
     *
     * @param string $sTable        = Name of the Table
     * @param array  $aColumns      = Array with TableColumns for delete
     * @param bool   $bCleanDbCache = Should clean the DB Cache?
     */
    public static function deleteTableColumn(
        string $sTable = '',
        array $aColumns = [],
        bool $bCleanDbCache = false
    ): void {
        if (count($aColumns)) {
            foreach ($aColumns as $sColumn => $sColumnDescription) {
                $sDelete = "alter table {$sTable} drop {$sColumn}";
                self::execute($sDelete);
            }
            if ($bCleanDbCache) {
                self::_cleanDbCache($sTable);
            }
        }
    }

    /**
     * insert Columns in Table table.
     *
     * @param string $sTable        = Name of the Table
     * @param array  $aColumns      = Array with TableColumns for insert array
     *                              [0 => ['columnname' => ..., 'definition' => ..., 'comment' => ..., 'suffix' => ...]]
     * @param bool   $bCleanDbCache = Should clean the DB Cache?
     */
    public static function insertTableColumn(
        string $sTable = '',
        array $aColumns = [],
        bool $bCleanDbCache = false
    ): void {
        if (count($aColumns)) {
            foreach ($aColumns as $aColumnDescription) {
                $sSql = "alter table {$sTable} add `" . $aColumnDescription['columnname'] . '` ' .
                    $aColumnDescription['definition'] . " comment '" .
                    $aColumnDescription['comment'] . "' " .
                    $aColumnDescription['suffix'];
                self::execute($sSql);
            }
            if ($bCleanDbCache) {
                self::_cleanDbCache($sTable);
            }
        }
    }

    /**
     * get the field Size of a table column.
     *
     * @param string $sTable = Name of the Table
     * @param string $sField = Name of the Table-Field
     *
     * @throws DatabaseConnectionException
     */
    public static function getFieldSize(string $sTable, string $sField): string
    {
        $sDbName = Registry::getConfig()->getShopConfVar('dbName');
        $oDb = DatabaseProvider::getDb();
        $sSql = 'select character_maximum_length
            from information_schema.columns
            where table_schema = ' . $oDb->quote($sDbName) . '
            and table_name = ' . $oDb->quote($sTable) . '
            and column_name = ' . $oDb->quote($sField);

        return $oDb->getOne($sSql);
    }

    /**
     * get the name of Tables with a special prefix.
     *
     * @param string $sTable = Name of the Table
     *
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    public static function getTableWithPrefix(string $sTable): array
    {
        $aResult = [];

        $oDb = DatabaseProvider::getDb();
        $sSql = "show tables like '{$sTable}%'";

        if ($aTables = $oDb->getAll($sSql)) {
            foreach ($aTables as $aTable) {
                $aResult[] = $aTable[0];
            }
        }

        return $aResult;
    }

    /**
     * set the field Size of a table column.
     *
     * @param string $sTable = Name of the Table
     * @param string $sField = Name of the Table-Field
     * @param int    $iSize  = new Size of the column
     */
    public static function setFieldSize(string $sTable, string $sField, int $iSize): void
    {
        $sSql = 'alter table ' . $sTable . '
            modify ' . $sField . '
            varchar(' . $iSize . ')';
        self::execute($sSql);
    }

    /**
     * get the field Informations of a table column.
     *
     * @param string $sTable       = Name of the Table
     * @param string $sField       = Name of the Table-Field
     * @param string $sInformation = e.g. column_type, comments etc.
     *
     * @throws DatabaseConnectionException
     */
    public static function getFieldInformation(
        string $sTable,
        string $sField,
        string $sInformation = 'column_type'
    ): string {
        $sDbName = Registry::getConfig()->getShopConfVar('dbName');
        $oDb = DatabaseProvider::getDb();
        $sSql = 'select ' . $sInformation . '
            from information_schema.columns
            where table_schema = ' . $oDb->quote($sDbName) . '
            and table_name = ' . $oDb->quote($sTable) . '
            and column_name = ' . $oDb->quote($sField);

        return $oDb->getOne($sSql);
    }

    /**
     * modified the field of a table column.
     *
     * @param string $sTable   = Name of the Table
     * @param string $sField   = Name of the Table-Field
     * @param string $sType    = New Type
     * @param string $sDefault = New Default
     * @param string $sComment = New Comment, if empty, trying to keep the old comment (optional)
     * @param bool   $bNotNull = Null? (optional)
     *
     * @throws DatabaseConnectionException
     */
    public static function changeField(
        string $sTable,
        string $sField,
        string $sType,
        string $sDefault,
        string $sComment = '',
        bool $bNotNull = true
    ): void {
        if (empty($sComment)) {
            $sComment = self::getFieldInformation($sTable, $sField, 'column_comment');
        }

        $sSql = 'alter table ' . $sTable . '
            modify ' . $sField . ' ' . $sType . ' ' .
            ($bNotNull ? 'not null' : '') .
            ($sDefault ? " default '" . $sDefault . "' " : '') .
            ($sComment ? " comment '" . $sComment . "' " : '');
        self::execute($sSql);
    }

    /**
     * execute a SQL. On Error catch an Exception.
     *
     * @param string $sSql = Name of the Table
     */
    public static function execute(string $sSql): int
    {
        $iResult = 0;

        try {
            $iResult = DatabaseProvider::getDb()->execute($sSql);
        } catch (Exception $e) {
            Registry::getUtilsView()->addErrorToDisplay($e->getMessage());
        }

        return $iResult;
    }

    /**
     * create an Single-Table SQL for Updates or Inserts
     * Use it only for simple update functions, without dependencies.
     * When it comes to a speed advantage.
     * Otherwise, always perform the updates object oriented.
     *
     * @param string $sTable       = Name of the Table
     * @param array  $aData        = array with Data for Updates
     * @param string $sAction      = SQL-Action (insert|update)
     *                             [field1 => value1, field2 => value2]
     * @param array  $aConditions  = array with Data for the Conditions
     *                             [
     *                             [
     *                             'operator' => 'and|or',
     *                             'condition' => '=|>|<|>=|<=',
     *                             'field' => field1,
     *                             'value' => 'value1',
     *                             'noquote' => true
     *                             ],
     *                             [
     *                             'operator' => 'and|or',
     *                             'condition' => '=|>|<|>=|<=',
     *                             'field' => field2,
     *                             'value' => 'value2',
     *                             'noquote' => false
     *                             ]
     *                             ]
     * @param bool   $bCheckShopId = check also the shopid in the SQL?
     *
     * @throws DatabaseConnectionException
     */
    public static function createSingleTableExecuteSql(
        string $sTable = '',
        array $aData = [],
        string $sAction = 'insert',
        array $aConditions = [],
        bool $bCheckShopId = true
    ): string {
        $sResult = '';

        if (
            $sTable
            && count($aData)
            && in_array($sAction, ['insert', 'update'])
        ) {
            $sSql = ($sAction === 'update' ? $sAction : 'insert into');

            $sSql .= " {$sTable} set ";

            array_walk($aData, static function (&$mValue, $mKey) {
                if (
                    Str::getStr()->strtolower($mValue) !== 'now()'
                    && (
                        !is_numeric($mValue)
                        || (
                            is_numeric($mValue)
                            && strpos((string)$mValue, '0') !== false
                        )
                    )
                ) {
                    $mValue = DatabaseProvider::getDb()->quote($mValue);
                }
                $mValue = " {$mKey} = " . $mValue;
            });

            $sSql .= implode(', ', $aData);

            $sCondition = '';

            if (is_array($aConditions) && count($aConditions)) {
                foreach ($aConditions as $aCondition) {
                    // check if all necessary keys exists
                    if (
                        count(
                            array_diff_key(
                                array_flip(
                                    ['operator', 'condition', 'field', 'value', 'noquote']
                                ),
                                $aCondition
                            )
                        ) === 0
                    ) {
                        $sCondition .= ' ' . $aCondition['operator'];
                        $sCondition .= " {$aCondition['field']} " . $aCondition['condition'] . ' ' .
                            (
                                $aCondition['noquote'] ?
                                $aCondition['value'] :
                                DatabaseProvider::getDb()->quote($aCondition['value'])
                            );
                    }
                }
            }

            if ($sCondition) {
                if ($bCheckShopId) {
                    $sCondition .= ' and `oxshopid` = ' . Registry::getConfig()->getShopId();
                }

                $sSql .= ' where 1 ' . $sCondition;
            }
            $sResult = $sSql;
        }

        return $sResult;
    }

    /**
     * clean the DB Cache.
     *
     * @param string $sTable = Name of the Table
     */
    private static function _cleanDbCache(string $sTable = ''): void
    {
        if ($sTable) {
            $oMetaData = oxNew(DbMetaDataHandler::class);
            $oMetaData->updateViews([$sTable]);

            $aFiles = glob(
                Registry::getUtils()->getCacheFilePath(
                    null,
                    true
                ) . 'oxpec_' . $sTable . '_allfields_*.txt'
            );
            if (is_array($aFiles)) {
                foreach ($aFiles as $sFile) {
                    @unlink($sFile);
                }
            }
        }
    }
}
