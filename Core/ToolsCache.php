<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Module\ModuleList;
use OxidEsales\Eshop\Core\Module\ModuleVariablesLocator;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ShopConfigurationDaoBridgeInterface;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\DataObject\ModuleConfiguration;
use OxidEsales\EshopCommunity\Internal\Framework\Module\State\ModuleStateServiceInterface;

class ToolsCache
{
    /** the cache clean */
    public static function cleanCache(): void
    {
        $oStr = Str::getStr();
        // TMP-Files
        $sTmpDir = Registry::getConfig()->getConfigParam('sCompileDir') . '/';
        $sTmpDir = $oStr->preg_replace('/(\\/)+/', '/', $sTmpDir);
        if (is_dir($sTmpDir)) {
            ToolsFile::deleteFilesFromPath($sTmpDir);
        }

        // Smarty-Files
        $sSmartyDir = Registry::getUtilsView()->getSmartyDir() . '/';
        $sSmartyDir = $oStr->preg_replace('/(\\/)+/', '/', $sSmartyDir);
        if (is_dir($sSmartyDir)) {
            ToolsFile::deleteFilesFromPath($sSmartyDir);
        }
    }

    /** the image reseter */
    public static function resetImages(): void
    {
        ToolsFile::deleteRecursively(
            Registry::getConfig()->getPicturePath(null) . 'generated/',
            false
        );
    }

    /** the update views */
    public static function updateViews(): void
    {
        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();
    }

    /** clean dynamic Seo-Urls */
    public static function cleanDynamicSeoUrls(): void
    {
        $sSql = "delete from `oxseo` where `oxtype` != 'static'";
        ToolsDB::execute($sSql);
    }

    /** reset tpl blocks */
    public static function resetTplBlocks(): void
    {
        foreach (Registry::getConfig()->getShopIds() as $iShopId) {
            $sSql = 'delete
                from `oxtplblocks`
                where oxid in
                (
                    select min(e.`oxid`)
                    from (
                        select * from `oxtplblocks`
                        where `oxshopid` = ' . $iShopId . '
                    ) e
                    group by `oxmodule`, `oxtheme`, `oxfile`, `oxblockname`,
                     `oxtemplate`, `oxpos`, `oxshopid`, `oxactive`
                    having count(*) > 1
                )';
            ToolsDB::execute($sSql);
        }

        $aReallyExistsModules = array_flip(self::_getReallyExistsModules());

        if (count($aReallyExistsModules)) {
            $sSql = 'delete
                from `oxtplblocks`
                where oxmodule not in (' .
                implode(
                    ', ',
                    DatabaseProvider::getDb()->quoteArray($aReallyExistsModules)
                )
                . ')';
            ToolsDB::execute($sSql);
        }
    }

    /** cleanup the Modules.
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public static function cleanUpModules(): void
    {
        $oConfig = Registry::getConfig();
        $oModuleList = oxNew(ModuleList::class);

        // OXID Standard CleanUp
        $oModuleList->cleanup();

        // collect ModuleOptionsKeys
        $aModuleOptionsKeys = [
            'aModule' . $oModuleList::MODULE_KEY_PATHS,
            'aModule' . $oModuleList::MODULE_KEY_EVENTS,
            'aModule' . $oModuleList::MODULE_KEY_VERSIONS,
            'aModule' . $oModuleList::MODULE_KEY_EXTENSIONS,
            'aModule' . $oModuleList::MODULE_KEY_FILES,
            'aModule' . $oModuleList::MODULE_KEY_TEMPLATES,
            'aModule' . $oModuleList::MODULE_KEY_CONTROLLERS,
            'aDisabledModules',
            'moduleSmartyPluginDirectories',
        ];

        $aReallyExistsModules = self::_getReallyExistsModules();
        $aModulesInConfig = self::_getModulesInConfig();

        foreach ($oConfig->getShopIds() as $iShopId) {
            // cleanUp ModuleOptions
            foreach ($aModuleOptionsKeys as $sKey) {
                $aModuleOption = $oConfig->getShopConfVar($sKey, $iShopId, '');
                $aNewModuleOption = [];
                foreach ($aModuleOption as $sModuleId => $aOptions) {
                    if ($sModuleId && array_key_exists($sModuleId, $aReallyExistsModules)) {
                        $aNewModuleOption[$sModuleId] = $aOptions;
                    }
                }
                ToolsConfig::saveConfigParam($sKey, $aNewModuleOption, 'aarr', '', $iShopId);
            }

            // remove extended not exists classes
            $aNotExistsModules = array_diff($aModulesInConfig, $aReallyExistsModules);

            if (count($aNotExistsModules)) {
                $aExtensionsToDelete = [];
                foreach ($aNotExistsModules as $sModuleId) {
                    $aExtensionsToDelete += $oModuleList->getModuleExtensions($sModuleId);
                }

                $aUpdatedExtensions = $oModuleList->diffModuleArrays($aNotExistsModules, $aExtensionsToDelete);
                $aUpdatedExtensionsChains = $oModuleList->buildModuleChains($aUpdatedExtensions);
                ToolsConfig::saveConfigParam('aModules', $aUpdatedExtensionsChains, 'aarr');
            }

            // remove module-options from not exists Modules
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSql = "select `oxmodule`
                from `oxconfig`
                where `oxmodule` like 'module:%'
                and `oxshopid` = " . $iShopId . '
                group by oxmodule';
            $aModulesInConfig = $oDb->getAll($sSql);

            foreach ($aModulesInConfig as $aModuleInConfig) {
                $sModuleId = substr_replace($aModuleInConfig['oxmodule'], '', 0, strlen('module:'));
                if (!array_key_exists($sModuleId, $aReallyExistsModules)) {
                    $sSql = 'delete from `oxconfig`
                        where `oxmodule` = ' . $oDb->quote('module:' . $sModuleId) . '
                        and `oxshopid` = ' . $iShopId;
                    ToolsDB::execute($sSql);

                    $sSql = 'delete from `oxconfigdisplay`
                        where `oxcfgmodule`= ' . $oDb->quote('module:' . $sModuleId);
                    ToolsDB::execute($sSql);
                }
            }
        }

        // Remove from caches.
        ModuleVariablesLocator::resetModuleVariables();
    }

    protected static function _getReallyExistsModules(): array
    {
        // search for really exists Modules
        $aModulesInConfig = self::_getModulesInConfig();

        $aReallyExistsModules = [];
        foreach ($aModulesInConfig as $sModuleId => $sModulePath) {
            if (is_dir(Registry::getConfig()->getModulesDir() . $sModulePath)) {
                $aReallyExistsModules[$sModuleId] = $sModulePath;
            }
        }

        return $aReallyExistsModules;
    }

    protected static function _getModulesInConfig(): array
    {
        $aModulesModulesInConfig = [];

        $container = ContainerFactory::getInstance()->getContainer();

        $moduleConfigurations = $container
            ->get(ShopConfigurationDaoBridgeInterface::class)
            ->get()
            ->getModuleConfigurations();

        $activeModules = [];

        $moduleStateService = $container->get(ModuleStateServiceInterface::class);

        foreach ($moduleConfigurations as $moduleConfiguration) {
            $aModulesModulesInConfig[$moduleConfiguration->getId()] = $moduleConfiguration->getPath();
        }
        return $aModulesModulesInConfig;
    }
}
