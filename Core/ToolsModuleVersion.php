<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use JsonException;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Facts\Facts;
use Symfony\Component\Filesystem\Path;

class ToolsModuleVersion
{
    /**
     * Gets the module version based on the module ID.
     *
     * @param string $moduleId Modul-ID (z. B. 'my_module').
     * @return string The module version or `null` if not found.
     */
    public static function getModuleVersion(string $moduleId): string
    {
        try {
            $modulePath = self::getModulePath($moduleId);
            if (!$modulePath) {
                return '';
            }

            $namespace = self::getNamespaceFromComposerJson($modulePath);
            if (!$namespace) {
                return '';
            }

            $version = self::getVersionFromLockfile($namespace)
                ?: self::getVersionFromComposerJson($modulePath)
                    ?: '-';

            return self::cleanVersionTag($version);
        } catch (Exception) {
            return '';
        }
    }

    /**
     * Gets the theme version based on the theme ID.
     *
     * @param string $themeId Theme-ID (z. B. 'my_theme').
     * @return string The theme version or `` if not found.
     */
    public static function getThemeVersion(string $themeId): string
    {
        $version = self::getVersionFromLockfileByTargetDirectory($themeId)
            ?: '-';

        return self::cleanVersionTag($version);
    }

    /**
     * Determines the path of the module using the module ID.
     *
     * @param string $moduleId
     * @return string The path of the module or `null` if not found.
     */
    private static function getModulePath(string $moduleId): string
    {
        $modulePath = '';
        if (function_exists('oxNew')) {
            $module = oxNew(Module::class);
            $modulePath = Registry::getConfig()->getModulesDir() .
                $module->getModulePath($moduleId);
        }

        return is_dir($modulePath) ? $modulePath : '';
    }

    /**
     * Gets the module version from composer.lock based on the namespace.
     *
     * @param string $namespace
     * @return string
     */
    private static function getVersionFromLockfile(string $namespace): string
    {
        $packages = self::getPackagesFromLockFile();

        foreach ($packages as $package) {
            if ($package['name'] === $namespace) {
                return $package['version'] ?? '';
            }
        }

        return '';
    }

    /**
     * Gets the version from composer.lock based on the target-directory.
     *
     * @param string $targetDirectory
     * @return string
     */
    private static function getVersionFromLockfileByTargetDirectory(string $targetDirectory): string
    {
        $packages = self::getPackagesFromLockFile();
        foreach ($packages as $package) {
            if (
                isset($package['extra']['oxideshop']['target-directory'])
                && $package['extra']['oxideshop']['target-directory'] === $targetDirectory
            ) {
                return $package['version'] ?? '';
            }
        }
        return '';
    }

    /**
     * Gets the packages from composer.lock
     *
     * @return array []
     */
    private static function getPackagesFromLockFile(): array
    {
        $lockFile = self::getLockfilePath();
        if (!file_exists($lockFile)) {
            return [];
        }

        try {
            $composerLock = json_decode(file_get_contents($lockFile), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            return [];
        }

        if (!$composerLock || !isset($composerLock['packages'])) {
            return [];
        }

        return array_merge($composerLock['packages'], $composerLock['packages-dev']);
    }

    /**
     * Gets the namespace from the module's composer.json.
     *
     * @param string $modulePath
     * @return string The namespace or `null` if not found.
     */
    private static function getNamespaceFromComposerJson(string $modulePath): string
    {
        $composerFile = $modulePath . '/composer.json';

        if (file_exists($composerFile)) {
            try {
                $composerData = json_decode(file_get_contents($composerFile), true, 512, JSON_THROW_ON_ERROR);
            } catch (JsonException) {
                return '';
            }
            return $composerData['name'] ?? ''; // 'name' corresponds to the namespace
        }

        return '';
    }

    /**
     * Gets the module version from the module's composer.json.
     *
     * @param string $modulePath
     * @return string
     */
    private static function getVersionFromComposerJson(string $modulePath): string
    {
        $composerFile = $modulePath . '/composer.json';

        if (file_exists($composerFile)) {
            try {
                $composerData = json_decode(file_get_contents($composerFile), true, 512, JSON_THROW_ON_ERROR);
            } catch (JsonException) {
                return '';
            }
            return $composerData['version'] ?? '';
        }

        return '';
    }

    /**
     * Gets the path to the composer.lock file.
     *
     * @return string
     */
    private static function getLockfilePath(): string
    {
        $facts = new Facts();
        return Path::join($facts->getShopRootPath(), 'composer.lock');
    }

    /**
     * Removes a single letter from the beginning of the version if it stands alone.
     *
     * @param string $version The original version.
     * @return string The cleaned up version.
     */
    private static function cleanVersionTag(string $version): string
    {
        $result = preg_replace('/^[a-zA-Z](?=\d)/', '', $version);
        return $result ?? '';
    }
}