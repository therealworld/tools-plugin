<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use SimpleXMLElement;
use XMLReader;
use XmlWriter;

class ToolsXML
{
    /** the XML Writer obj */
    protected static ?object $_oXMLWriter = null;

    /** the XML Reader obj */
    protected static ?object $_oXMLReader = null;

    /** the Export File */
    protected static string $_sXMLFileName = '';

    /** the Encoding of XML-File */
    protected static string $_sXMLEncoding = 'UTF-8';

    /** the Version of XML-File */
    protected static string $_sXMLVersion = '1.0';

    /** a optional File Path */
    protected static string $_sXMLFilePath = '';

    /** a possible XML String */
    protected static string $_sXMLString = '';

    /** array with XML Objects */
    protected static array $_aXMLObjects = [];

    /** the Start Node */
    protected static string $_sStartNode = '';

    /** the Start Node Attributes */
    protected static array $_aStartNodeAttributes = [];

    /** sanitize the XML instead using the CData-Wrapper */
    protected static bool $_bSanitizeInsteadUseCDataWrapper = false;

    /**
     * the pathtypes for possible xml-file-places.
     *
     * @param array
     */
    protected static array $_aPathTypes = [
        'import', 'export',
    ];

    /**
     * the vars for find positions, offsets etc.
     *
     * @param int
     */
    protected static ?int $_iCount = null;
    protected static ?int $_iPos = null;

    /**
     * set the XML file name.
     *
     * @param string $sXMLFileName - the file name
     * @param string $sXMLFilePath - optional file path
     */
    public static function setXMLFileName(
        string $sXMLFileName = '',
        string $sXMLFilePath = ''
    ): void {
        self::$_sXMLFileName = $sXMLFileName;
        self::$_sXMLFilePath = $sXMLFilePath;
    }

    /**
     * get the XML file name.
     *
     * @param bool   $bWithPath - the name included with path?
     * @param string $sPathType - 'import', 'export' Decides between the paths
     */
    public static function getXMLFileName(
        bool $bWithPath = false,
        string $sPathType = ''
    ): string {
        $mResult = '';
        if (self::$_sXMLFileName) {
            if ($bWithPath && in_array($sPathType, self::$_aPathTypes, true)) {
                $oConfig = Registry::getConfig();

                switch ($sPathType) {
                    case 'import':
                        // if we have a individual path, so we take this path
                        // else we take the compile path
                        $mResult .= (
                            self::$_sXMLFilePath ?
                            $oConfig->getConfigParam('sShopDir') . self::$_sXMLFilePath :
                            $oConfig->getConfigParam('sCompileDir')
                        );

                        break;

                    case 'export':
                        $mResult .= $oConfig->getConfigParam('sShopDir') . self::$_sXMLFilePath;

                        break;
                }
            }
            $mResult .= self::$_sXMLFileName;
        }

        return $mResult;
    }

    /**
     * set the XML content - only use for small content.
     *
     * @param string $sString    - the xml content
     * @param bool   $bOverwrite - overwrite the StartNode?
     */
    public static function setXMLString(
        string $sString = '',
        bool $bOverwrite = false
    ): void {
        if ((!self::$_sXMLString || $bOverwrite) && $sString) {
            self::$_sXMLString = $sString;
        }
    }

    /**
     * get the XML content.
     */
    public static function getXMLString(): string
    {
        return self::$_sXMLString;
    }

    /**
     * get the XML content.
     */
    public static function getXMLObj(): array
    {
        return self::$_aXMLObjects;
    }

    /**
     * reset the XML content.
     */
    public static function resetXMLObj(): void
    {
        self::$_aXMLObjects = [];
    }

    /**
     * set the XML start node.
     *
     * @param string $sStartNode - the start node
     * @param bool   $bOverwrite - overwrite the StartNode?
     */
    public static function setStartNode(
        string $sStartNode = '',
        bool $bOverwrite = false
    ): void {
        if ((!self::$_sStartNode || $bOverwrite) && $sStartNode) {
            self::$_sStartNode = $sStartNode;
        }
    }

    /**
     * set the XML start node Attributes.
     *
     * @param array $aStartNodeAttributes - the start node attributes
     * @param bool  $bOverwrite           - overwrite the StartNode?
     */
    public static function setStartNodeAttributes(
        array $aStartNodeAttributes = [],
        bool $bOverwrite = false
    ): void {
        if (
            (count(self::$_aStartNodeAttributes) === 0 || $bOverwrite)
            && count($aStartNodeAttributes)
        ) {
            self::$_aStartNodeAttributes = $aStartNodeAttributes;
        }
    }

    /**
     * set XML Version.
     */
    public static function setXMLVersion(string $sXMLVersion = ''): void
    {
        self::$_sXMLVersion = $sXMLVersion;
    }

    /**
     * set XML Version.
     */
    public static function setXMLEncoding(string $sXMLEncoding = ''): void
    {
        self::$_sXMLEncoding = $sXMLEncoding;
    }

    /**
     * get the XML start node.
     */
    public static function getStartNode(): string
    {
        return self::$_sStartNode;
    }

    /**
     * get the XML start node attributes.
     */
    public static function getStartNodeAttributes(): array
    {
        return self::$_aStartNodeAttributes;
    }

    /**
     * get XMLFile from Array - this is a fast version for limited data
     * if you've more data do the steps alone.
     *
     * @param string $sXMLFileName         - the name of XML-File
     * @param string $sXMLFilePath         - optional file path
     * @param array  $aData                - the array with the data
     * @param string $sStartNode           - the start node
     * @param array  $aStartNodeAttributes - optional start node attributes
     * @param bool   $bSendToBrowser       - send the file to the browser
     * @param string $sXMLEncoding         - optinal overwrite the Standard "UTF-8"
     */
    public static function getXMLFileFromArray(
        string $sXMLFileName = '',
        string $sXMLFilePath = '',
        array $aData = [],
        string $sStartNode = '',
        array $aStartNodeAttributes = [],
        bool $bSendToBrowser = true,
        string $sXMLEncoding = ''
    ): string {
        self::setXMLEncoding($sXMLEncoding);
        self::setXMLFileName($sXMLFileName, $sXMLFilePath);
        self::setStartNode($sStartNode, true);
        self::setStartNodeAttributes($aStartNodeAttributes, true);
        self::setItemsToXML($aData);

        return self::getXMLFile($bSendToBrowser);
    }

    /**
     * get XMLString from Array - this is a fast version for limited data
     * if you've more data do the steps alone.
     *
     * @param array  $aData                - the array with the data
     * @param string $sStartNode           - the start node
     * @param array  $aStartNodeAttributes - optional start node attributes
     * @param string $sXMLEncoding         - optinal overwrite the Standard "UTF-8"
     */
    public static function getXMLStringFromArray(
        array $aData = [],
        string $sStartNode = '',
        array $aStartNodeAttributes = [],
        string $sXMLEncoding = ''
    ): string {
        self::setXMLEncoding($sXMLEncoding);
        self::setStartNode($sStartNode, true);
        self::setStartNodeAttributes($aStartNodeAttributes, true);
        self::setItemsToXML($aData);

        return self::getXMLString();
    }

    /**
     * get Array from XML - this is a fast version for limited data
     * if you've more data do the steps alone.
     *
     * @param string   $sXMLFileName - the name of XML-File
     * @param string   $sXMLFilePath - path to XML-File
     * @param string   $sStartNode   - the start node
     * @param null|int $iLimit       - the maximum of result-items - null = unlimited
     */
    public static function getArrayFromXMLFile(
        string $sXMLFileName = '',
        string $sXMLFilePath = '',
        string $sStartNode = '',
        ?int $iLimit = null
    ): array {
        $aResult = [];
        self::resetXMLObj();

        self::setXMLFileName($sXMLFileName, $sXMLFilePath);
        self::setStartNode($sStartNode, true);
        if (self::collectItemsFromXML($iLimit)) {
            $aResult = array_map('self::convertXMLToArray', self::getXMLObj());
        }

        return $aResult;
    }

    /**
     * get one Item from XML - the best way togehter with API-Answers.
     *
     * @param string   $sXMLString - the XMLString
     * @param string   $sStartNode - the start node
     * @param null|int $iLimit     - the maximum of result-items - null = unlimited
     */
    public static function getObjectFromXMLString(
        string $sXMLString = '',
        string $sStartNode = '',
        ?int $iLimit = null
    ): array {
        $aResult = [];
        self::resetXMLObj();

        self::setXMLString($sXMLString, true);
        self::setStartNode($sStartNode, true);
        if (self::collectItemsFromXML($iLimit)) {
            $aResult = self::getXMLObj();
        }

        return $aResult;
    }

    /**
     * get one Item from XML - the best way togehter with API-Answers.
     *
     * @param string   $sXMLString - the XMLString
     * @param string   $sStartNode - the start node
     * @param null|int $iLimit     - the maximum of result-items - null = unlimited
     */
    public static function getArrayFromXMLString(
        string $sXMLString = '',
        string $sStartNode = '',
        ?int $iLimit = null
    ): array {
        $aResult = self::getObjectFromXMLString($sXMLString, $sStartNode, $iLimit);

        return array_map('self::convertXMLToArray', $aResult);
    }

    /**
     * get one Item from XML - the best way togehter with API-Answers.
     *
     * @param string $sXMLString - the XMLString
     * @param string $sStartNode - the start node
     */
    public static function getOneItemFromXMLString(
        string $sXMLString = '',
        string $sStartNode = ''
    ): ?object {
        return self::getObjectFromXMLString($sXMLString, $sStartNode, 1)[0];
    }

    /**
     * get the XML Content - maybe as Download.
     */
    public static function getXMLFile(bool $bWithHeader = true): string
    {
        $sResult = '';
        if (($sFile = self::getXMLFileName(true, 'export')) && file_exists($sFile)) {
            if ($bWithHeader) {
                header('Content-type: text/xml');
                header('Content-Description: File Transfer');
                header('Content-Disposition: attachment; filename="' . self::getXMLFileName() . '"');
                header('Content-Type: application/octet-stream');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($sFile));
                readfile($sFile);
                Registry::getUtils()->showMessageAndExit('');
            } else {
                $sResult = file_get_contents($sFile);
            }
        }

        return $sResult;
    }

    /**
     * collect Items from a XML-File or a XML-String.
     *
     * @param null|int $iLimit  - the numbers of Elements the maximal return, null = no limit
     * @param int      $iOffset - the offset of iterated Elements
     * @param bool     $bLast   - last element?
     */
    public static function collectItemsFromXML(
        ?int $iLimit = 20,
        int $iOffset = 0,
        bool $bLast = false
    ): bool {
        $bResult = false;

        $sNodeName = self::getStartNode();
        $sString = self::getXMLString();
        $sFile = self::getXMLFileName(true, 'import');

        if ($sNodeName && ($sString || $sFile)) {
            self::$_iPos = 0;
            self::resetXMLObj();
            if ($sFile) {
                $sCheck = $sFile;
                $isFile = true;
            } else {
                $sCheck = $sString;
                $isFile = false;
            }
            $bCheck = self::_loadAndValidateXML($sCheck, $isFile);

            if ($bCheck) {
                self::$_iCount = 0;

                // move to the first node
                while (self::$_oXMLReader->read() && self::$_oXMLReader->name !== $sNodeName);

                self::resetXMLObj();

                while (self::$_oXMLReader->name === $sNodeName) {
                    // found offset
                    if (self::$_iPos < $iOffset) {
                        self::$_iPos++;
                        self::$_oXMLReader->next($sNodeName);

                        continue;
                    }

                    if (
                        $oXmlResult = simplexml_load_string(
                            self::$_oXMLReader->readOuterXML(),
                            'SimpleXMLElement',
                            LIBXML_NOCDATA
                        )
                    ) {
                        $bResult = true;

                        self::$_aXMLObjects[] = $oXmlResult;

                        self::$_iCount++;
                        self::$_iPos++;

                        // break by limit
                        if (!is_null($iLimit) && self::$_iCount > $iLimit) {
                            break;
                        }
                    }

                    // go to next Node
                    self::$_oXMLReader->next($sNodeName);
                }
            }

            // only we have no result or it is the last, we close the XML
            if (!$bResult || $bLast) {
                self::$_oXMLReader->close();
            }
        }

        return $bResult;
    }

    /**
     * set Items to a XML-File.
     *
     * @param array $aData  - the array with the data
     * @param bool  $bFirst - has the data array the first element inside?
     * @param bool  $bLast  - has the data array the last element inside?
     */
    public static function setItemsToXML(
        array $aData = [],
        bool $bFirst = true,
        bool $bLast = true
    ): bool {
        $bResult = false;
        if ($sStartNode = self::getStartNode()) {
            $bNeedOpenXML = false;

            if (is_null(self::$_oXMLWriter) || $bFirst) {
                self::$_oXMLWriter = new XmlWriter();
                $bNeedOpenXML = true;
            }

            $sFile = self::getXMLFileName(true, 'export');

            if (
                $bFirst
                && $sFile
                && file_exists($sFile)
                && is_file($sFile)
            ) {
                // if file exists and we start with creating, we delete the file
                unlink($sFile);
            }
            if ($bNeedOpenXML) {
                if ($sFile) {
                    $bOpenXMLSuccess = self::$_oXMLWriter->openURI($sFile);
                } else {
                    $bOpenXMLSuccess = self::$_oXMLWriter->openMemory();
                }

                self::$_oXMLWriter->setIndent(true);

                self::$_oXMLWriter->startDocument(self::$_sXMLVersion, self::$_sXMLEncoding);
                self::$_oXMLWriter->startElement($sStartNode);
                if ($aStartNodeAttributes = self::getStartNodeAttributes()) {
                    foreach ($aStartNodeAttributes as $sAttribute => $sValue) {
                        self::$_oXMLWriter->startAttribute($sAttribute);
                        self::$_oXMLWriter->text($sValue);
                        self::$_oXMLWriter->endAttribute();
                    }
                }
            } else {
                $bOpenXMLSuccess = true;
            }

            if ($bOpenXMLSuccess && count($aData)) {
                $bResult = true;
                self::_writeXMLElement($aData);
                if ($sFile) {
                    self::$_oXMLWriter->flush();
                }
            }

            // in the last step we close the xml structure
            if ($bLast) {
                $bResult = true;

                self::$_oXMLWriter->endElement();
                self::$_oXMLWriter->endDocument();
                // if we have no file we get the memory-buffer
                if (!$sFile) {
                    self::$_sXMLString = self::$_oXMLWriter->outputMemory();
                }
            }
        }

        return $bResult;
    }

    /**
     * convert a XML-Object to an array.
     *
     * @param SimpleXMLElement $oXml - SimpleXMLElement XML-Object
     */
    public static function convertXMLToArray(SimpleXMLElement $oXml): array
    {
        $aResult = [];
        $oNodes = $oXml->children();
        $oAttributes = $oXml->attributes();

        if (count($oAttributes) !== 0) {
            foreach ($oAttributes as $sAttrName => $sAttrValue) {
                $aResult['@attributes'][$sAttrName] = (string) $sAttrValue;
            }
        }

        if ($oNodes->count() === 0) {
            if ($oXml->attributes()) {
                $aResult['value'] = (string) $oXml;
            } else {
                $aResult[] = (string) $oXml;
            }

            return $aResult;
        }

        foreach ($oNodes as $oNodeName => $oNodeValue) {
            if (count($oNodeValue->xpath('../' . $oNodeName)) < 2) {
                $aResult[$oNodeName] = self::_reduceResult(self::convertXMLToArray($oNodeValue));

                continue;
            }

            $aResult[$oNodeName][] = self::_reduceResult(self::convertXMLToArray($oNodeValue));
        }

        return $aResult;
    }

    /**
     * reduce an array with only one int-indexed element to string.
     *
     * @return array|string
     */
    protected static function _reduceResult(array $array)
    {
        return count($array) === 1 && $array === array_values($array) ? $array[0] : $array;
    }

    /**
     * Write XML as per Associative Array.
     *
     * @param array $aData - Associative Data Array
     */
    protected static function _writeXMLElement(array $aData = []): bool
    {
        foreach ($aData as $mKey => $mValue) {
            if (is_array($mValue) && range(0, count($mValue) - 1) === array_keys($mValue)) {
                // numeric array
                self::$_oXMLWriter->startElement($mKey);
                $iCount = count($mValue) - 1;
                foreach ($mValue as $iKey => $mItemValue) {
                    if (is_array($mItemValue)) {
                        self::_writeXMLElement($mItemValue);
                        if (($iKey !== $iCount) && count($mItemValue) > 1) {
                            self::$_oXMLWriter->endElement();
                            self::$_oXMLWriter->startElement($mKey);
                        }
                    } else {
                        self::_wrapCData((string) $mItemValue, $mKey);
                    }
                }
                self::$_oXMLWriter->endElement();
            } elseif (is_array($mValue)) {
                // associative array
                self::$_oXMLWriter->startElement($mKey);
                self::_writeXMLElement($mValue);
                self::$_oXMLWriter->endElement();
            } else {
                if (Str::getStr()->strpos((string) $mKey, '@') !== false) {
                    self::$_oXMLWriter->writeAttribute(substr($mKey, 1), $mValue);
                } else {
                    $normalizedKey = !is_numeric($mKey) ? $mKey : '';
                    self::_wrapCData((string) $mValue, $normalizedKey);
                }
            }
        }

        return true;
    }

    /**
     * Wrap the Text with CDATA if needed
     *
     * @param string $sString - string to wrap
     * @param string $sNode   - optional XML Node Name
     */
    protected static function _wrapCData(string $sString, string $sNode = ''): void
    {
        // Convert only the minimum required characters for XML
        $sString = self::_makeXMLSafeMinimal($sString);
        $bCDataNeeded = self::_requiresCDATA($sString);

        if ($sNode) {
            if ($bCDataNeeded && !self::$_bSanitizeInsteadUseCDataWrapper) {
                self::$_oXMLWriter->startElement($sNode);
                self::$_oXMLWriter->writeCData($sString);
                self::$_oXMLWriter->endElement();
            } else {
                $sString = self::_sanitizeForXML($sString);
                self::$_oXMLWriter->writeElement($sNode, $sString);
            }
        } else {
            if ($bCDataNeeded && !self::$_bSanitizeInsteadUseCDataWrapper) {
                self::$_oXMLWriter->writeCData($sString);
            } else {
                $sString = self::_sanitizeForXML($sString);
                self::$_oXMLWriter->text($sString);
            }
        }
    }

    /**
     * Check if CData Wrapper ist needed.
     *
     * @param string $sString - string to wrap
     */
    protected static function _requiresCDATA(string $sString): bool
    {
        // List of known XML entities (we focus on the essentials for this test)
        $validEntities = array_merge(
            ['&lt;', '&gt;', '&amp;', '&apos;', '&quot;'],
            array_map(static fn($i) => "&#$i;", range(0, 127)) // Extended ASCII range
        );

        // 1. Quick check for CDATA sequences
        if (str_contains($sString, ']]>') || str_contains($sString, '<![CDATA[')) {
            return true;
        }

        // 2. Check for unescaped special characters (ignoring parts that are valid entities)
        if (preg_match('/[<>&\'"](?!(?:amp|lt|gt|apos|quot);|#\d+;)/', $sString, $matches)) {
            return true;
        }

        // 3. Validate all entities
        if (preg_match_all('/&[^;]*;|&[^;]*$/', $sString, $matches)) {
            foreach ($matches[0] as $entity) {
                // Check for incomplete entity
                if (!str_ends_with($entity, ';')) {
                    return true;
                }

                // Validate numeric entity
                if (str_starts_with($entity, '&#')) {
                    $num = substr($entity, 2, -1);
                    if (!ctype_digit($num)) {
                        return true;
                    }
                    // Number is valid - no need to check range as it's already in validEntities
                    continue;
                }

                // Check named entity
                if (!in_array($entity, $validEntities, true)) {
                    return true;
                }
            }
        }

        // All checks passed
        return false;
    }

    /**
     * Convert only the minimum required characters for XML
     * The Rest should be done via CDATA-Wrapping
     *
     * @param string $sString - string to wrap
     */
    protected static function _makeXMLSafeMinimal(string $sString): string
    {
        return str_replace(
            ['&',     '<',    '>',    '"',      "'"],
            ['&amp;', '&lt;', '&gt;', '&quot;', '&apos;'],
            $sString
        );
    }

    /**
     * sanitize a string, so that it can be used instead of CDATA-Wrapping
     *
     * @param string $sString - string to wrap
     */
    protected static function _sanitizeForXML(string $sString): string
    {
        // 1. Remove CDATA markers if present
        $sString = str_replace([']]>', '<![CDATA['], '', $sString);

        // 2. Fix incomplete entities (remove & if not followed by valid entity)
        $sString = (string) preg_replace('/&(?![a-zA-Z]+;|#[0-9]+;)/', '', $sString);

        // 3. Remove invalid numeric entities
        $sString = preg_replace('/&#[^0-9;]+;/', '', $sString);

        // 4. Remove unknown/invalid named entities
        $validNamedEntities = ['amp', 'lt', 'gt', 'apos', 'quot'];
        return (string) preg_replace_callback('/&([a-zA-Z]+);/', function($matches) use ($validNamedEntities) {
            return in_array($matches[1], $validNamedEntities, true) ? $matches[0] : '';
        }, $sString);
    }

    /**
     * load and validate XML (not DTD valid).
     *
     * @param string $sXML    - string or file to check
     * @param bool   $bIsFile - is the string a filename?
     */
    protected static function _loadAndValidateXML(string $sXML = '', bool $bIsFile = true): bool
    {
        if (is_null(self::$_oXMLReader)) {
            self::$_oXMLReader = new XMLReader();
        }

        if ($bIsFile) {
            // we check if files exist
            if ($bResult = file_exists($sXML)) {
                self::$_oXMLReader->open($sXML, self::$_sXMLEncoding, LIBXML_NOCDATA);
            }
        } else {
            $bResult = true;
            self::$_oXMLReader->xml($sXML, self::$_sXMLEncoding, LIBXML_NOCDATA);
        }

        if ($bResult) {
            self::$_oXMLReader->setParserProperty(XMLReader::VALIDATE, true);
            $bResult = self::$_oXMLReader->isValid();
            self::$_oXMLReader->setParserProperty(XMLReader::VALIDATE, false);
        }

        return $bResult;
    }
}
