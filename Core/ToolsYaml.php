<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Symfony\Component\Yaml\Yaml;

class ToolsYaml
{
    /**
     * return Yaml Content.
     */
    public static function getYamlContent(string $sYaml): array
    {
        $aResult = [];
        if ($sYaml) {
            $aResult = (array) Yaml::parse($sYaml, Yaml::PARSE_CUSTOM_TAGS);
        }

        return $aResult;
    }

    /**
     * get Yaml File.
     */
    public static function getYamlStringFromFile(string $sYamlFile): string
    {
        $sYaml = '';
        if (file_exists($sYamlFile)) {
            $sYaml = file_get_contents($sYamlFile);
        }

        return $sYaml;
    }

    /**
     * set Yaml File.
     */
    public static function setDataToYamlFile(string $sYamlFile, array $aData = []): void
    {
        if ($sYamlFile && $sYaml = self::getYamlFromArray($aData)) {
            file_put_contents(
                $sYamlFile,
                $sYaml
            );
        }
    }

    /**
     * get Yaml From Array.
     */
    public static function getYamlFromArray(array $aData = []): string
    {
        return Yaml::dump($aData, 5, 2);
    }
}
