<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class ToolsLang
{
    /**
     * try to translate a string.
     *
     * @param string $sString - string to translate
     * @param string $sPrefix - possible prefix for language variables
     */
    public static function translateString(string $sString = '', string $sPrefix = ''): string
    {
        $oLang = Registry::getLang();
        $sTrans = str_replace('%s', '', $sString);
        $sTrans = trim(preg_replace('/[^a-zA-Z]/', ' ', $sTrans));
        $sTrans = ToolsString::deleteManyWhitespaces($sTrans);
        $sTrans = str_replace(' ', '_', $sTrans);
        $sTrans = $sPrefix . '_' . Str::getStr()->strtoupper($sTrans);
        $sTrans = (string) $oLang->translateString($sTrans);
        if ($oLang->isTranslated()) {
            $sString = $sTrans;
        }

        return $sString;
    }
}
