<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\DynamicImageGenerator;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

/**
 * Extended Image generator class.
 */
class ToolsDynamicImageGenerator extends DynamicImageGenerator
{
    /** class instance. */
    protected static ?ToolsDynamicImageGenerator $_instance = null;
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->_aConfParamToPath = array_merge(
            $this->_aConfParamToPath,
            $this->getConfFromTrwTools('aTRWToolsDynImgConfParamToPath')
        );
    }

    /** Returns object instance */
    public static function getInstance(): ToolsDynamicImageGenerator
    {
        if (static::$_instance === null) {
            static::$_instance = oxNew(self::class);
        }

        return static::$_instance;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _getImageMasterPath()
    {
        $sPath = '';
        $aDetection = $this->getConfFromTrwTools('aTRWToolsDynImgDetectWithTwoOptions');

        $sUri = $this->_getImageUri();

        $bFoundDynImageWithTwoOptions = false;

        foreach ($aDetection as $sDetection) {
            if (stripos($sUri, $sDetection) !== false) {
                $bFoundDynImageWithTwoOptions = true;

                break;
            }
        }

        if ($sUri && ($sPath = dirname($sUri, 2))) {
            $oStr = Str::getStr();
            if ($bFoundDynImageWithTwoOptions) {
                $sPath = $oStr->preg_replace('/\\/([^\\/]*)\\/([^\\/]*)$/', '/master/\\2/', $sPath);
            } else {
                $sPath = $oStr->preg_replace('/\\/([^\\/]*)\\/([^\\/]*)\\/([^\\/]*)$/', '/master/\\2/\\3/', $sPath);
            }
        }

        return $sPath;
    }

    /**
     * Generates Image and returns its location on file system
     *
     * @param string $source image source
     * @param string $target image target
     * @param int $width image width
     * @param int $height image height
     * @param int $quality
     * @return void
     */
    public function generateTRWImage(string $source, string $target, int $width, int $height, int $quality): void
    {
        // including generator files
        includeImageUtils();

        $fileExtensionSource = strtolower(pathinfo($source, PATHINFO_EXTENSION));
        switch ($fileExtensionSource) {
            case "png":
                $generatedImagePath = $this->_generatePng($source, $target, $width, $height);
                break;
            case "jpeg":
            case "jpg":
                $generatedImagePath = $this->_generateJpg($source, $target, $width, $height, $quality);
                break;
            case "gif":
                $generatedImagePath = $this->_generateGif($source, $target, $width, $height);
                break;
        }
    }

    private function getConfFromTrwTools(string $sVarName = ''): array
    {
        $aConf = [];
        if ($sVarName && ToolsConfig::getModuleExists('trwtools')) {
            $aConf = (array) Registry::getConfig()->getConfigParam(
                $sVarName
            );
        }

        return $aConf;
    }
}
