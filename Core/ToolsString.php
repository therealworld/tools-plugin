<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Html2Text\Html2Text;
use Isbn\Exception;
use Isbn\Isbn;
use OxidEsales\Eshop\Core\MailValidator;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class ToolsString
{
    /** Object for ISBN-Library */
    protected static ?Isbn $_oISBN = null;

    /** array with problematic Utf8-encoded chars in Iso-encoding */
    protected static array $_aProblemEnc = [
        'euro'   => 'EURO',
        'hellip' => '...',
        'dagger' => '+',
        'permil' => '%o',
        'sbquo'  => "'",
        'lsaquo' => "'",
        'lsquo'  => "'",
        'rsquo'  => "'",
        'bdquo'  => '"',
        'ldquo'  => '"',
        'rdquo'  => '"',
        'raquo'  => '"',
        'laquo'  => '"',
        'bull'   => '*',
        'middot' => '*',
        'ndash'  => '-',
        'mdash'  => '-',
        'trade'  => ' TM',
        'brvbar' => '|',
        'copy'   => ' (c)',
        'reg'    => ' (r)',
        'plusmn' => '+/-',
        'micro'  => 'micro',
        'para'   => '',
        'nbsp'   => ' ',
        'deg'    => '',
    ];

    /**
     * convert a string to ascii, needed for email-creating.
     *
     * @param string $sString           - the to be converted string
     * @param array  $aReplace          - special replaces Array
     * @param bool   $bForEMailAdresses - if it set, only a-z, 0-9, /_|+ - are converted to $sDelimiter
     * @param string $sDelimiter        - the central delimiter instead of different delimiters
     */
    public static function convertTextToAscii(
        string $sString = '',
        array $aReplace = [],
        bool $bForEMailAdresses = true,
        string $sDelimiter = '_',
        string $sUtf8RegionCode = 'de_DE'
    ): string {
        $oStr = Str::getStr();

        setlocale(LC_ALL, $sUtf8RegionCode . '.UTF8');
        if (!empty($aReplace)) {
            $sString = str_replace($aReplace, ' ', $sString);
        }
        $sClean = iconv('UTF-8', 'ASCII//TRANSLIT', $sString);
        if ($bForEMailAdresses) {
            $sClean = $oStr->preg_replace('/[^a-zA-Z0-9\\/_|+ -]/', '', $sClean);
            $sClean = $oStr->strtolower(trim($sClean, '-'));
            $sClean = $oStr->preg_replace('/[\\/_|+ -]+/', $sDelimiter, $sClean);
        } else {
            $sClean = $oStr->preg_replace("/[^\x01-\x7F]/", '', $sClean);
        }

        return $sClean;
    }

    /**
     * convert a string to normal chars, all special chars are delete.
     *
     * @param string $sString    - the to be converted string
     * @param string $sDelimiter - the central delimiter instead of different delimiters
     */
    public static function convertTextToNormalChars(
        string $sString,
        string $sDelimiter = ' ',
        string $sExcludePregReplace = '',
        bool $bToLower = false
    ): string {
        $oStr = Str::getStr();

        $sResult = $sString;
        $sResult = self::convertHtmlToText($sResult);
        $sResult = $oStr->preg_replace('/[^\p{L}\p{N} ' . $sExcludePregReplace . ']/u', ' ', $sResult);
        $sResult = self::deleteManyWhitespaces($sResult);
        $sResult = str_replace(' ', $sDelimiter, $sResult);
        if ($bToLower) {
            $sResult = $oStr->strtolower($sResult);
        }

        return $sResult;
    }

    /**
     * convert a Html-String to Text.
     */
    public static function convertHtmlToText(string $sString = ''): string
    {
        return (new Html2Text($sString))->getText();
    }

    /**
     * clean a ISBN and convert a ISBN10 to ISBN13 if it is possible.
     *
     * @param string $sISBN       - ISBN
     * @param bool   $bWithHyphen - add Hyphens
     *
     * @throws Exception
     */
    public static function cleanISBN(string $sISBN = '', bool $bWithHyphen = false): string
    {
        $sResult = '';
        if (is_null(self::$_oISBN)) {
            self::$_oISBN = new Isbn();
        }
        // remove hyphens by myself because
        // of some troubles with strlen in ISBN-Library
        $sISBN = preg_replace('/[^0-9x]/i', '', $sISBN);

        if ($sISBN && (self::$_oISBN->check->identify($sISBN) !== false)) {
            if (self::$_oISBN->check->is10($sISBN) && self::$_oISBN->validation->isbn10($sISBN)) {
                $sISBN = self::$_oISBN->translate->to13($sISBN);
            } elseif (!(self::$_oISBN->check->is13($sISBN) && self::$_oISBN->validation->isbn13($sISBN))) {
                $sISBN = '';
            }

            if ($bWithHyphen && $sISBN) {
                $sISBN = self::$_oISBN->hyphens->addHyphens($sISBN);
            }

            $sResult = $sISBN;
        }

        return $sResult;
    }

    /**
     * delete Many Whitespaces to only one.
     *
     * @param bool $bAlsoHtml - also delete html whitespaces &nbsp;
     */
    public static function deleteManyWhitespaces(string $sString = '', bool $bAlsoHtml = false): string
    {
        if ($bAlsoHtml) {
            $sString = str_replace('&nbsp;', ' ', $sString);
        }

        $sString = Str::getStr()->preg_replace(
            ["/[\\s\t\n\r\\s]+/", '/\\s([?.!])/'],
            [' ', '$1'],
            $sString
        );

        return str_replace("\xc2\xa0", ' ', $sString);
    }

    /** is the string a valid eMail? */
    public static function isValidEmail(string $sMail = ''): bool
    {
        return oxNew(MailValidator::class)->isValidEmail($sMail);
    }

    /** is the string a valid MySQL Date? */
    public static function isValidMySqlDate(string $sDate = ''): bool
    {
        return Str::getStr()->preg_match(
            '/^(\\d{4})-(\\d{2})-(\\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/',
            $sDate,
            $matches
        )
            && checkdate($matches[2], $matches[3], $matches[1]);
    }

    /** check if a string is utf-8 encoded.
     *
     * @param string $sString - the to be checked string
     */
    public static function isUtf8(string $sString = ''): bool
    {
        return mb_detect_encoding($sString . ' ', 'UTF-8', true) === 'UTF-8';
    }

    /**
     * convert a Text-String to Utf8.
     */
    public static function encodeUtf8(string $sString = '', string $sBaseEncoding = 'ISO-8859-15')
    {
        if (!self::isUtf8($sString)) {
            $sString = mb_convert_encoding($sString, 'UTF-8', $sBaseEncoding);
            // $sString = utf8_encode($sString);
        }

        return $sString;
    }

    /**
     * remove the UTF8-BOM from String (https://en.wikipedia.org/wiki/Byte_order_mark#UTF-8).
     */
    public static function removeUtf8BOM(string $sString): string
    {
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (Str::getStr()->strpos($sString, $bom) === 0) {
            $sString = substr($sString, 3);
        }

        return $sString;
    }

    /**
     * split a String on the last occurrence of a splitter.
     *
     * @param string       $sString      - string to split
     * @param array|string $mSplitter    - splitter - string or array
     * @param bool         $bReturnAfter - return the Rest after the splitting
     */
    public static function splitOnLastOccurrence(
        string $sString = '',
        $mSplitter = null,
        bool $bReturnAfter = true
    ): string {
        $sResult = $sString;

        if (!is_array($mSplitter)) {
            $mSplitter = [$mSplitter];
        }

        if ($sString) {
            foreach ($mSplitter as $sSplitter) {
                if ($iFoundSplitter = strrpos($sString, $sSplitter)) {
                    $iLength = $bReturnAfter ? $iFoundSplitter : null;
                    $offset = $bReturnAfter ? $iFoundSplitter + Str::getStr()->strlen($sSplitter) : 0;
                    $sResult = trim(substr($sString, $offset, $iLength));

                    break;
                }
            }
        }

        return $sResult;
    }

    /**
     * format a platform (like Google) conform price with a decimal "point".
     */
    public static function formatPlatformPrice(string $sString): string
    {
        $oCurrency = Registry::getConfig()->getActShopCurrencyObject();

        return number_format((float) $sString, $oCurrency->decimal, '.', '') . ' ' . $oCurrency->name;
    }

    /**
     * enhanced UTF8-Decoder.
     *
     * @param string $sString - string to decode
     */
    public static function decodeUtf8(string $sString = ''): string
    {
        if (self::isUtf8($sString)) {
            // Problem is that utf8_decode convert HTML chars for &bdquo; and other to ? or &nbsp;
            // to \xA0. And you can not replace, that they are in some char bytes and you broke
            // cyrillic (or other alphabet) chars.
            $sString = mb_convert_encoding($sString, 'HTML-ENTITIES', 'UTF-8');
            $sString = Str::getStr()->preg_replace_callback(
                '#(?<!\&ETH;)\&(' . implode('|', array_keys(self::$_aProblemEnc)) . ');#s',
                'self::_callBackProblemEnc',
                $sString
            );
            $sString = mb_convert_encoding($sString, 'UTF-8', 'HTML-ENTITIES');
            $sString = utf8_decode($sString);
        }

        return $sString;
    }

    /**
     * Checks if a value exists in an array case insensitive.
     *
     * @param string $sNeedle   - The searched value
     * @param array  $aHaystack - The array
     */
    public static function inArrayI(string $sNeedle, array $aHaystack): bool
    {
        return in_array(Str::getStr()->strtolower($sNeedle), array_map('strtolower', $aHaystack), true);
    }

    /**
     * stripTags method with blacklist.
     *
     * @param array $aBlacklist - array with htmlelements that should remove,
     *                          the elements without lace braces
     *                          e.g. $aBlacklist = ['br', 'p', 'div']
     */
    public static function stripTagsWithBlacklist(string $sString, array $aBlacklist = []): string
    {
        if (count($aBlacklist)) {
            $sBlacklist = implode('|', $aBlacklist);
            $sString = preg_replace(
                '/<\/?(' . $sBlacklist . ')\/?>/m',
                '',
                $sString
            );
        }

        return $sString;
    }

    /**
     * truncate a string
     *
     * @param string $sStr - string to truncate
     * @param int $iLength - length of string max
     * @param string $sEtc - the glued etc
     * @param bool $bBreakWords - break words?
     * @param bool $bTruncateInTheMiddle - the truncated part in the middle of the string?
     */
    public static function truncate(
        string $sStr,
        int $iLength = 80,
        string $sEtc = '...',
        bool $bBreakWords = false,
        bool $bTruncateInTheMiddle = false
    ): string {
        if ($iLength === 0) {
            return '';
        }

        if (strlen($sStr) > $iLength) {
            $iLength -= min($iLength, strlen($sEtc));
            if (!$bBreakWords && !$bTruncateInTheMiddle) {
                $sStr = preg_replace('/\s+?(\S+)?$/', '', substr($sStr, 0, $iLength + 1));
            }
            if (!$bTruncateInTheMiddle) {
                return substr($sStr, 0, $iLength) . $sEtc;
            } else {
                return substr($sStr, 0, $iLength / 2) . $sEtc . substr($sStr, - $iLength / 2);
            }
        } else {
            return $sStr;
        }
    }

    /**
     * callBack function for preg_replace_callback in above method decodeUtf8.
     */
    protected static function _callBackProblemEnc(array $aMatches): string
    {
        return self::$_aProblemEnc[$aMatches[1]];
    }
}
