<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Application\Model\Shop;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Module\ModuleList;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\ShopVersion;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setting\Setting;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ToolsConfig
{
    /** Possible Moduls Option Types */
    protected static array $_aPossibleOptionTypes = ['select', 'bool', 'num', 'str', 'arr', 'aarr'];

    /** exclude internal Shop Configs */
    protected static array $_aExcludeInternalShopConfigs = [
        'activeModules', 'aModuleaModuleControllers', 'aModuleaModuleEvents', 'aModuleaModuleExtensions',
        'aModuleaModuleFiles', 'aModuleaModulePaths', 'aModuleaModuleTemplates', 'aModuleaModuleVersions',
        'aModuleaModuleVersions', 'aModuleEvents', 'aModuleExtensions', 'aModuleFiles', 'aModulePaths',
        'aModules', 'aModuleTemplates', 'aModuleVersions', 'aMultiLangTables', 'aServersData',
        'moduleSmartyPluginDirectories', 'aModuleControllers', 'aDisabledModules',
    ];

    /** exclude Shop Values from shoptable */
    protected static array $_aExcludeShopTableValues = [
        'oxid', 'oxadbutlerid', 'oxaffilinetid', 'oxsuperclicksid', 'oxedition', 'oxversion',
        'oxaffiliweltid', 'oxaffili24id', 'oxtimestamp',
    ];

    /**
     * Upload a File.
     *
     * @param string $sRequestFileName   - Name of the request-Filename
     * @param string $sRequestFileTarget - Name of the request-File Target
     * @param bool   $bResultWithPath    - the Result is a file name. Result with path?
     */
    public static function uploadFile(
        string $sRequestFileName = '',
        string $sRequestFileTarget = '',
        bool $bResultWithPath = false
    ): string {
        $sResult = '';
        $oConfig = Registry::getConfig();

        if ($sRequestFileName) {
            $aFile = $oConfig->getUploadedFile($sRequestFileName);
            if (isset($aFile['name']) && $aFile['name']) {
                $sFileUploadTarget = '';
                if ($sRequestFileTarget) {
                    $sFileUploadTarget .= $oConfig->getConfigParam('sShopDir') . $sRequestFileTarget;
                }
                if (!$sFileUploadTarget || !is_dir($sFileUploadTarget)) {
                    $sFileUploadTarget = $oConfig->getConfigParam('sCompileDir');
                }
                $sFileName = basename($aFile['name']);
                move_uploaded_file($aFile['tmp_name'], $sFileUploadTarget . $sFileName);
                $sResult = ($bResultWithPath ? $sFileUploadTarget : '') . $sFileName;
            }
        }

        return $sResult;
    }

    /**
     * Save Admin Options.
     *
     * @param string $sRequestArrayName - Name of the request-Parameter
     * @param array  $aRequestFiles     - Array for handling of fileuploads
     *                                  (e.g.: [0 => [
     *                                  'name' => 'varname of <input type="file" name="...">',
     *                                  'target' => 'shop root relative target dir',
     *                                  'varname' => 'varname of config, where the filename are pushed'
     *                                  ]];
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function saveAdminOptions(string $sRequestArrayName = '', array $aRequestFiles = []): void
    {
        $oRequest = Registry::getRequest();

        $aOptions = $oRequest->getRequestParameter($sRequestArrayName);

        if (is_array($aRequestFiles)) {
            foreach ($aRequestFiles as $aRequestFile) {
                if (isset($aRequestFile['name'], $aRequestFile['target'])) {
                    $sUploadResult = self::uploadFile($aRequestFile['name'], $aRequestFile['target']);
                    if (
                        isset($aRequestFile['varname'])
                        && is_array($aOptions)
                        && $sUploadResult
                    ) {
                        $aOptions[$aRequestFile['varname']] = $sUploadResult;
                    }
                }
            }
        }

        if (is_array($aOptions)) {
            // The name of the variable controls the correct storage of the options
            // Example: bool.ModuleName.VarName
            // 1) type (bool, num, str, arr, aarr)
            // 2) the Name of the Module
            // 3) the Name of the Variable
            foreach ($aOptions as $sKey => $mVarVal) {
                [$sVarType, $sModule, $sVarName] = array_pad(explode('.', $sKey), 3, '');
                if (in_array($sVarType, self::$_aPossibleOptionTypes, true)) {
                    // prepare array options
                    if (in_array($sVarType, ['arr', 'aarr'])) {
                        if (!is_array($mVarVal)) {
                            $mVarVal = explode("\n", $mVarVal);

                            if ($sVarType === 'aarr') {
                                $mVarValNew = [];
                                foreach ($mVarVal as $sLine) {
                                    $sLine = trim($sLine);
                                    if ($sLine !== '' && Str::getStr()->preg_match('/(.+)=>(.+)/', $sLine, $aRegs)) {
                                        $sKey = trim($aRegs[1]);
                                        $sVal = trim($aRegs[2]);
                                        if ($sKey !== '' && $sVal !== '') {
                                            $mVarValNew[$sKey] = $sVal;
                                        }
                                    }
                                }
                                $mVarVal = $mVarValNew;
                            }
                        }
                        $mVarVal = array_filter($mVarVal);
                    }

                    self::saveConfigParam($sVarType . $sModule . $sVarName, $mVarVal, $sVarType, $sModule);
                }
            }
        }
    }

    /**
     * Save a var.
     *
     * @param string   $sName    - name of Variable
     * @param mixed    $mVarVal  - Values
     * @param string   $sVarType - Valuetype
     * @param string   $sModule  - Modulename
     * @param null|int $iShopId  - ShopId
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function saveConfigParam(
        string $sName,
        $mVarVal,
        string $sVarType = 'str',
        string $sModule = '',
        ?int $iShopId = null
    ): void {
        $oConfig = Registry::getConfig();
        $oConfig->setConfigParam($sName, $mVarVal);

        if (!$sVarType) {
            $sVarType = (string) ToolsDB::getAnyId('oxconfig', ['oxvarname' => $sName], 'oxvartype');
        }

        if (!$sModule) {
            $sModule = (string) ToolsDB::getAnyId('oxconfig', ['oxvarname' => $sName], 'oxmodule');
        }

        // add prefix if not exists
        $sModuleWithPrefix = $sModule;
        if ($sModule && !strpos($sModule, ':')) {
            $sModuleWithPrefix = 'module:' . $sModule;
        }
        $sModuleWithPrefix = Str::getStr()->strtolower($sModuleWithPrefix);

        // remove prefix
        $sModule = preg_replace('/^[^:]*:\s*/', '', $sModule);
        $sModule = Str::getStr()->strtolower($sModule);

        // save Module Options additional in OXID >= v6.2 as Bridge-Configuration
        if (
            !self::isDBConfig()
            && strpos($sModuleWithPrefix, 'module:') !== false
        ) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                ModuleConfigurationDaoBridgeInterface::class
            );
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sName);
            $oModuleSetting->setValue($mVarVal);
            $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }

        $oConfig->saveShopConfVar(
            $sVarType,
            $sName,
            $mVarVal,
            $iShopId,
            $sModuleWithPrefix
        );
    }

    /**
     * Return if a Module exsists in a needed Version.
     *
     * @param string $sModule  - Modulename
     * @param string $sVersion - Version
     * @param bool   $bActive  - is it necessary that the module is active?
     */
    public static function getModuleExists(string $sModule = '', string $sVersion = '', bool $bActive = true): bool
    {
        $bResult = false;
        if ($sModule) {
            $oModuleList = oxNew(ModuleList::class);
            $aModules = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_VERSIONS);
            if (array_key_exists($sModule, $aModules)) {
                if (
                    !$sVersion
                    || version_compare($aModules[$sModule], $sVersion, '>=')
                ) {
                    $bResult = true;
                }
                if ($bResult && $bActive) {
                    $aDisabledModules = Registry::getConfig()->getConfigParam('aDisabledModules');
                    if (is_array($aDisabledModules) && in_array($sModule, $aDisabledModules, true)) {
                        $bResult = false;
                    }
                }
            }
        }

        return $bResult;
    }

    /**
     * get a Options Array and validate it.
     *
     * @param string $sOptionsArrayName - The Name of the option
     */
    public static function getOptionsArray(string $sOptionsArrayName = ''): array
    {
        $aOptions = Registry::getConfig()->getConfigParam($sOptionsArrayName);

        return is_array($aOptions) ? $aOptions : [];
    }

    /** Returns the Default Nr. of Items (e.g. articles) per page */
    public static function getDefaultNrOfItemsPerPage(): int
    {
        $iNrOfItemsPerPage = 0;
        $oConfig = Registry::getConfig();
        $sListType = $oConfig->getConfigParam('sDefaultListDisplayType');
        $aNrOfItemsPerPage = (
            ($sListType === 'grid') ?
            $oConfig->getConfigParam('aNrofCatArticlesInGrid') :
            $oConfig->getConfigParam('aNrofCatArticles')
        );
        if (is_array($aNrOfItemsPerPage)) {
            $iNrOfItemsPerPage = (int) array_shift($aNrOfItemsPerPage);
        }

        return $iNrOfItemsPerPage === 0 ? 10 : $iNrOfItemsPerPage;
    }

    /** set Locale */
    public static function setLocale(): void
    {
        $oLang = Registry::getLang();
        $sLocaleCode = $oLang->getLanguageAbbr() . '_' . Str::getStr()->strtoupper($oLang->getLanguageAbbr());
        setlocale(LC_ALL, $sLocaleCode);
    }

    /**
     * add a Value to the Constraint of a Module Option select.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'module:bsimport')
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function addOptionToShopConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        if ($sCfgVarname && $sCfgModule && $sOption && !self::isDBConfig()) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                ModuleConfigurationDaoBridgeInterface::class
            );
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aConstraints = $oModuleSetting->getConstraints();
            if (!in_array($sOption, $aConstraints, true)) {
                $aConstraints[] = $sOption;
                $oModuleSetting->setConstraints($aConstraints);
                $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
            }
        }

        self::addOptionToDBConfigConstraint($sCfgVarname, $sCfgModule, $sOption);
    }

    /**
     * add a Value to the Constraint of a DB Module Option select (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     */
    public static function addOptionToDBConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        if ($sCfgVarname && $sCfgModule && $sOption) {
            $aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false);
            $aConstraint = is_array($aConstraint) ? $aConstraint : [];
            if (!in_array($sOption, $aConstraint, true)) {
                $aConstraint[] = $sOption;
            }
            self::setDBConfigConstraint(
                $sCfgVarname,
                $sCfgModule,
                $aConstraint
            );
        }
    }

    /**
     * add a Value to a array Module Option.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function addOptionToShopConfigArray(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        array $aOptions = [],
        string $sArrayType = 'arr'
    ): void {
        if ($sCfgModule && !self::isDBConfig()) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                ModuleConfigurationDaoBridgeInterface::class
            );
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aConfig = self::_addOptionsToArray((array) $oModuleSetting->getValue(), $aOptions, $sArrayType);

            $oModuleSetting->setValue($aConfig);
            $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }

        self::addOptionToDBConfigArray(
            $sCfgVarname,
            $sCfgModule,
            $aOptions,
            $sArrayType
        );
    }

    /**
     * add a Value to a array Module DB Option (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function addOptionToDBConfigArray(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        array $aOptions = [],
        string $sArrayType = 'arr'
    ): void {
        if ($sCfgVarname && $aOptions && in_array($sArrayType, ['arr', 'aarr'])) {
            $oConfig = Registry::getConfig();
            $aConfig = (array) $oConfig->getConfigParam($sCfgVarname);

            $aConfig = self::_addOptionsToArray($aConfig, $aOptions, $sArrayType);

            self::saveConfigParam($sCfgVarname, $aConfig, $sArrayType, $sCfgModule);
        }
    }

    /**
     * remove a Value from the Constraint of a Module Option select.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function removeOptionFromShopConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        if (self::isDBConfig()) {
            self::removeOptionFromDBConfigConstraint($sCfgVarname, $sCfgModule, $sOption);
        }

        if ($sCfgVarname && $sCfgModule && $sOption) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                ModuleConfigurationDaoBridgeInterface::class
            );
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aConstraints = $oModuleSetting->getConstraints();
            $aConstraints = array_diff($aConstraints, [$sOption]);
            $oModuleSetting->setConstraints($aConstraints);
            $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }
    }

    /**
     * remove a Value from the Constraint of a Module DB-Option select (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     */
    public static function removeOptionFromDBConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        if ($sOption) {
            if ($aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false)) {
                if (($iKey = array_search($sOption, $aConstraint, true)) !== false) {
                    unset($aConstraint[$iKey]);
                }
                self::setDBConfigConstraint(
                    $sCfgVarname,
                    'module:' . $sCfgModule,
                    $aConstraint
                );
            }
        }
    }

    /**
     * remove a Value from the Constraint of a Theme Option select.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     */
    public static function removeOptionFromShopConfigThemeConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        self::removeOptionFromDBConfigThemeConstraint(
            $sCfgVarname,
            $sCfgModule,
            $sOption
        );
    }

    /**
     * remove a Value from the Constraint of a Theme DB-Option select (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     */
    public static function removeOptionFromDBConfigThemeConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = ''
    ): void {
        if ($sOption) {
            if ($aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false)) {
                if (($iKey = array_search($sOption, $aConstraint)) !== false) {
                    unset($aConstraint[$iKey]);
                }
                self::setDBConfigConstraint(
                    $sCfgVarname,
                    'theme:' . $sCfgModule,
                    $aConstraint
                );
            }
        }
    }

    /**
     * remove a Value from a array Module Option.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param string $sOption     - if we have an "arr" $sOption is the value,
     *                            if we have an "aarr" $sOption is the Key or the value
     * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
     */
    public static function removeOptionFromShopConfigArray(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = '',
        string $sArrayType = 'arr'
    ): void {
        // there is no better way for OXID 6.2, so we use the old method too
        self::removeOptionFromDBConfigArray(
            $sCfgVarname,
            $sCfgModule,
            $sOption,
            $sArrayType
        );
    }

    /**
     * remove a Value from a array Module DB-Option (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param string $sOption     - if we have an "arr" $sOption is the value,
     *                            if we have an "aarr" $sOption is the Key or the value
     * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function removeOptionFromDBConfigArray(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        string $sOption = '',
        string $sArrayType = 'arr'
    ): void {
        if ($sOption && in_array($sArrayType, ['arr', 'aarr'])) {
            $oConfig = Registry::getConfig();
            $aConfig = (array) $oConfig->getConfigParam($sCfgVarname);

            if ($sKey = ($sArrayType === 'aarr') ? $sOption : array_search($sOption, $aConfig, true)) {
                unset($aConfig[$sKey]);
                self::saveConfigParam($sCfgVarname, $aConfig, $sArrayType, $sCfgModule);
            }
        }
    }

    /**
     * get the Constraint of a Module Option.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param bool   $bAsString   - result as string or array
     *
     * @return mixed (array, string, boolean)
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getShopConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        bool $bAsString = true
    ) {
        if (self::isDBConfig() || (stripos($sCfgModule, 'theme:') !== false)) {
            return self::getDBConfigConstraint($sCfgVarname, $sCfgModule, $bAsString);
        }

        $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
            ModuleConfigurationDaoBridgeInterface::class
        );
        $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
        $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
        $mResult = $oModuleSetting->getConstraints();

        if ($bAsString) {
            $mResult = implode('|', $mResult);
        }

        return $mResult;
    }

    /**
     * get the Constraint of a Module DB-Option (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param bool   $bAsString   - result as string or array
     *
     * @return array|false|string
     */
    public static function getDBConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        bool $bAsString = true
    ) {
        $mResult = false;
        if ($sCfgVarname) {
            $aCheck = [
                'oxcfgvarname' => $sCfgVarname,
            ];

            if ($sCfgModule) {
                $aCheck['oxcfgmodule'] = $sCfgModule;
            }

            if (
                $sConstraint = ToolsDB::getAnyId(
                    'oxconfigdisplay',
                    $aCheck,
                    'oxvarconstraint'
                )
            ) {
                $aConstraint = array_filter(explode('|', $sConstraint));
                $mResult = $bAsString ? implode('|', $aConstraint) : $aConstraint;
            }
        }

        return $mResult;
    }

    /**
     * set the Constraint of a Module-Config.
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param mixed  $mConstraint - value as array or string
     */
    public static function setShopConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        $mConstraint = null
    ): void {
        if (self::isDBConfig() || (stripos($sCfgModule, 'theme:') !== false)) {
            self::setDBConfigConstraint($sCfgVarname, $sCfgModule, $mConstraint);

            return;
        }
        if ($sCfgVarname && $sCfgModule && $mConstraint) {
            if (is_string($mConstraint) && strpos($mConstraint, '|') !== false) {
                $mConstraint = explode('|', $mConstraint);
            }
            if (is_array($mConstraint)) {
                $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                    ModuleConfigurationDaoBridgeInterface::class
                );
                $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
                $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
                $oModuleSetting->setConstraints($mConstraint);
                $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
            }
        }
    }

    /**
     * set the Constraint of a DB-Module-Config (OXID < v6.2).
     *
     * @param string $sCfgVarname - name of variable
     * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
     * @param mixed  $mConstraint - value as array or string
     *
     * @return bool
     */
    public static function setDBConfigConstraint(
        string $sCfgVarname = '',
        string $sCfgModule = '',
        ?array $mConstraint = null
    ): void {
        $bResult = false;

        if ($sCfgVarname && $sCfgModule) {
            $mConstraint = is_array($mConstraint) ? implode('|', $mConstraint) : $mConstraint;

            if (!strpos($sCfgModule, ':')) {
                $sCfgModule = 'module:' . $sCfgModule;
            }

            $oDb = DatabaseProvider::getDb();
            $sSql = 'update `oxconfigdisplay`
                set `oxvarconstraint` = ' . $oDb->quote($mConstraint) . '
                where `oxcfgvarname` = ' . $oDb->quote($sCfgVarname) . '
                and `oxcfgmodule` = ' . $oDb->quote($sCfgModule);

            ToolsDB::execute($sSql);
        }
    }

    /**
     * set the Module-Definer of a Module-Config.
     *
     * @param string      $sCfgModule  - name of module-definer (e.g. 'trwexport')
     * @param string      $sCfgVarname - name of variable
     * @param null|string $sCfgGroup   - name of group
     * @param string      $sCfgType    - name of type
     * @param null|array  $aConstraint - possible Constraints
     * @param mixed       $mValue      - possible Value
     * @param null|int    $iShopId     - ShopId
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws NotFoundExceptionInterface
     */
    public static function createShopConfig(
        string $sCfgModule = '',
        string $sCfgVarname = '',
        ?string $sCfgGroup = '',
        string $sCfgType = 'str',
        array $aConstraint = [],
        $mValue = null,
        ?int $iShopId = null
    ): void {
        if (self::isDBConfig()) {
            self::createDBConfig(
                $sCfgModule,
                $sCfgVarname,
                $sCfgGroup,
                $sCfgType,
                $aConstraint,
                $mValue,
                $iShopId
            );
        }

        $bResult = false;

        if (
            $sCfgModule
            && $sCfgVarname
            && in_array($sCfgType, self::$_aPossibleOptionTypes, true)
        ) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
                ModuleConfigurationDaoBridgeInterface::class
            );
            if ($oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule)) {
                if (!$oModuleConfiguration->hasModuleSetting($sCfgVarname)) {
                    $oModuleSetting = new Setting();
                    $oModuleSetting->setName($sCfgVarname);
                    $oModuleSetting->setType($sCfgType);
                    $oModuleSetting->setGroupName($sCfgGroup);
                    if (is_array($aConstraint) && count($aConstraint)) {
                        $oModuleSetting->setConstraints($aConstraint);
                    }
                    if (!is_null($mValue)) {
                        $oModuleSetting->setValue($mValue);
                    }

                    $oModuleConfiguration->addModuleSetting($oModuleSetting);
                    $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
                }
            }
        }
    }

    /**
     * set the Module-Definer of a Module-Config.
     *
     * @param string      $sCfgModule  - name of module-definer (e.g. 'trwexport')
     * @param string      $sCfgVarname - name of variable
     * @param null|string $sCfgGroup   - name of group
     * @param string      $sCfgType    - name of type
     * @param mixed       $mValue      - possible Value
     * @param null|int    $iShopId     - ShopId
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws DatabaseConnectionException
     */
    public static function createDBConfig(
        string $sCfgModule = '',
        string $sCfgVarname = '',
        ?string $sCfgGroup = '',
        string $sCfgType = 'str',
        ?array $aConstraint = [],
        $mValue = null,
        ?int $iShopId = null
    ): void {
        if ($sCfgModule && !strpos($sCfgModule, ':')) {
            $sCfgModule = 'module:' . $sCfgModule;
        }

        $sCheckOxId = ToolsDB::getAnyId(
            'oxconfig',
            [
                'oxshopid'  => $iShopId,
                'oxvarname' => $sCfgVarname,
                'oxmodule'  => $sCfgModule,
            ]
        );

        $sCfgGroup = !is_null($sCfgGroup) ? $sCfgGroup : '';

        if (
            $sCfgModule
            && $sCfgVarname
            && !$sCheckOxId
            && in_array($sCfgType, self::$_aPossibleOptionTypes, true)
        ) {
            $sConstraint = !is_null($aConstraint) && count($aConstraint) ? implode('|', $aConstraint) : '';
            $sOxId = Registry::getUtilsObject()->generateUId();
            self::saveConfigParam($sCfgVarname, $mValue, $sCfgType, $sCfgModule, $iShopId);

            $oDb = DatabaseProvider::getDb();

            $sSql = 'delete from `oxconfigdisplay`
                where `oxcfgmodule` = ' . $oDb->quote($sCfgModule) . '
                and `oxcfgvarname` = ' . $oDb->quote($sCfgVarname);
            ToolsDB::execute($sSql);

            $sSql = 'insert into `oxconfigdisplay`
                (`OXID`, `OXCFGMODULE`, `OXCFGVARNAME`, `OXGROUPING`, `OXVARCONSTRAINT`)
                VALUES
                (' .
                    $oDb->quote($sOxId) . ',' .
                    $oDb->quote($sCfgModule) . ',' .
                    $oDb->quote($sCfgVarname) . ', ' .
                    $oDb->quote($sCfgGroup) . ', ' .
                    $oDb->quote($sConstraint) . '
                )';
            ToolsDB::execute($sSql);
        }
    }

    /**
     * set the Module-Definer of a Module-Config.
     *
     * @param string      $sCfgModule  - name of module-definer (e.g. 'trwexport')
     * @param string      $sCfgVarname - name of variable
     * @param null|string $sCfgGroup   - name of group
     * @param string      $sCfgType    - name of type
     * @param array       $aConstraint - possible Constraints
     * @param mixed       $mValue      - possible Value
     * @param null|int    $iShopId     - ShopId
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws DatabaseConnectionException
     */
    public static function updateDBConfig(
        string $sCfgModule = '',
        string $sCfgVarname = '',
        ?string $sCfgGroup = '',
        string $sCfgType = 'str',
        ?array $aConstraint = [],
        $mValue = null,
        ?int $iShopId = null
    ): void {
        $bResult = false;

        if ($sCfgModule && !strpos($sCfgModule, ':')) {
            $sCfgModule = 'module:' . $sCfgModule;
        }

        $sCheckOxId = ToolsDB::getAnyId(
            'oxconfig',
            [
                'oxshopid'  => $iShopId,
                'oxvarname' => $sCfgVarname,
                'oxmodule'  => $sCfgModule,
            ]
        );

        if (
            $sCfgModule
            && $sCfgVarname
            && $sCheckOxId
            && in_array($sCfgType, self::$_aPossibleOptionTypes, true)
        ) {
            self::saveConfigParam($sCfgVarname, $mValue, $sCfgType, $sCfgModule, $iShopId);
            self::setDBConfigConstraint($sCfgVarname, $sCfgModule, $aConstraint);
        } else {
            self::createDBConfig($sCfgModule, $sCfgVarname, $sCfgGroup, $sCfgType, $aConstraint, $mValue, $iShopId);
        }
    }

    /**
     * rename the Module-Group of a Module-Config.
     *
     * @param string $sOldName - name of group
     * @param string $sNewName - new name of group
     */
    public static function renameShopConfigThemeGroup(
        string $sOldName = '',
        string $sNewName = '',
        string $sThemeId = ''
    ): bool {
        return self::renameDBConfigThemeGroup($sOldName, $sNewName, $sThemeId);
    }

    /**
     * rename the Module-Group of a Module-DBConfig (OXID < v6.2).
     *
     * @param string $sOldName - name of group
     * @param string $sNewName - new name of group
     */
    public static function renameDBConfigThemeGroup(
        string $sOldName = '',
        string $sNewName = '',
        string $sThemeId = ''
    ): bool {
        $bResult = false;

        if ($sOldName && $sNewName && $sThemeId) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSql = 'update `oxconfigdisplay`
                set `oxgrouping` = ' . $oDb->quote($sNewName) . '
                where `oxcfgmodule` = ' . $oDb->quote('theme:' . $sThemeId) . '
                and `oxgrouping` = ' . $oDb->quote($sOldName);
            $bResult = ToolsDB::execute($sSql);
        }

        return $bResult;
    }

    /** check if it is a MultiShop-System */
    public static function isMultiShop(): bool
    {
        return !(Registry::getConfig()->getEdition() === 'CE');
    }

    /** get List of shopIds */
    public static function getShopIds(): array
    {
        $oConfig = Registry::getConfig();

        $aShopIds = [];
        if (!self::isMultiShop()) {
            $aShopIds[] = $oConfig->getShopId();
        } else {
            $aShopIds = $oConfig->getShopIds();
        }

        return $aShopIds;
    }

    /** check if config save in DB or newer */
    public static function isDBConfig(): bool
    {
        return version_compare(ShopVersion::getVersion(), '6.2.0', '<');
    }

    /**
     * get ShopConfigValues from Config.
     *
     * @param bool     $bInclude if true include the fields, else exclude them
     * @param null|int $iShopId  - ShopId
     * @param string   $sModule  - Config for module or theme ...
     *
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    public static function getShopConfigValues(
        array $aConfigFields = [],
        bool $bInclude = false,
        ?int $iShopId = null,
        string $sModule = ''
    ): array {
        $oConfig = Registry::getConfig();
        $iShopId ??= $oConfig->getShopId();

        if ($sModule) {
            $sSql = "SELECT c.`oxvarname`, c.`oxvartype`, %s as oxvarvalue,
                d.`oxgrouping`, d.`oxvarconstraint`, d.`oxpos`
                FROM `oxconfig` as c
                LEFT JOIN `oxconfigdisplay` as d 
                ON (c.`oxmodule` = d.`oxcfgmodule` and c.`oxvarname` = d.`oxcfgvarname`)
                WHERE c.`oxshopid` = '%s'
                AND c.`oxmodule` = " . DatabaseProvider::getDb()->quote($sModule) . '
                ORDER BY d.`oxgrouping` ASC, c.`oxvarname` ASC';
        } else {
            $sSql = "SELECT `oxvarname`, `oxvartype`, %s as oxvarvalue
                FROM `oxconfig`
                WHERE `oxshopid` = '%s'
                AND `oxmodule` = ''
                ORDER BY `oxvarname` ASC";
        }

        $sSql = sprintf(
            $sSql,
            $oConfig->getDecodeValueQuery(),
            $iShopId
        );

        $aConfigValues = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sSql);

        $aResult = [];
        foreach ($aConfigValues as $aConfigValue) {
            $sVarName = $aConfigValue['oxvarname'];
            $sVarType = $aConfigValue['oxvartype'];
            $mVarValue = $aConfigValue['oxvarvalue'];

            $bFound = false;
            foreach ($aConfigFields as $sConfigField) {
                if (stripos($sVarName, $sConfigField) !== false) {
                    $bFound = true;

                    break;
                }
            }
            if (($bInclude && !$bFound) || (!$bInclude && $bFound)) {
                continue;
            }

            if (in_array($sVarType, ['aarr', 'arr'])) {
                $mVarValue = unserialize($mVarValue, ['allowed_classes' => false]);
                $mVarValue = is_array($mVarValue) ? $mVarValue : [];
            } elseif ($sVarType === 'bool') {
                $mVarValue = (bool) $mVarValue;
            }

            $aResult[$sVarName] = [
                'type'  => $sVarType,
                'value' => $mVarValue,
            ];
            if ($sModule) {
                $aResult[$sVarName]['pos'] = (int) $aConfigValue['oxpos'];
                $aResult[$sVarName]['group'] = $aConfigValue['oxgrouping'];
                $aResult[$sVarName]['constraints'] = $aConfigValue['oxvarconstraint'];
            }
        }

        return $aResult;
    }

    /**
     * get ShopConfigValues from Config for Export.
     *
     * @param null|int $iShopId - ShopId
     * @param string   $sModule - Config for module or theme ...
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public static function getShopConfigValuesForExport(
        array $aExcludeConfigFields = [],
        ?int $iShopId = null,
        string $sModule = ''
    ): array {
        $aConfigFields = array_merge(self::$_aExcludeInternalShopConfigs, $aExcludeConfigFields);

        return self::getShopConfigValues(
            $aConfigFields,
            false,
            $iShopId,
            $sModule
        );
    }

    /**
     * get ShopConfigValues from Table for Export.
     *
     * @param null|int $iShopId - ShopId
     */
    public static function getShopTableValuesForExport(array $aExcludeConfigFields = [], ?int $iShopId = null): array
    {
        $oShop = oxNew(Shop::class);
        $oShop->load($iShopId);

        $oTableDesc = DatabaseProvider::getInstance()?->getTableDescription('oxshops');
        $aTableColDesc = [];
        foreach ($oTableDesc as $oTableColDesc) {
            $sName = Str::getStr()->strtolower($oTableColDesc->name);
            $sType = stripos($oTableColDesc->type, 'int') ? 'int' : 'str';
            $aTableColDesc[$sName] = $sType;
        }

        $aResult = [];
        $aExcludeConfigFields = array_merge($aExcludeConfigFields, self::$_aExcludeShopTableValues);
        $aMultiLingualFields = [];

        foreach ($oShop->getFieldNames() as $sFieldName) {
            if (in_array($sFieldName, $aExcludeConfigFields, true)) {
                continue;
            }
            $aResult[$sFieldName]['type'] = $aTableColDesc[$sFieldName];

            // collect multiLangFields
            if ($oShop->isMultilingualField($sFieldName)) {
                $aMultiLingualFields[] = $sFieldName;
            } else {
                $aResult[$sFieldName]['value'] = $oShop->getRawFieldData($sFieldName);
            }
        }

        foreach (Registry::getLang()->getLanguageIds() as $iLang => $sLangAbbr) {
            $oShop->loadInLang($iLang, $iShopId);
            foreach ($aMultiLingualFields as $sFieldName) {
                $aResult[$sFieldName]['value'][$sLangAbbr] = $oShop->getRawFieldData($sFieldName);
            }
        }

        return $aResult;
    }

    /**
     * add Values to a array.
     *
     * @param array  $aConfig    - array with Values
     * @param array  $aOptions   - array with new vales
     * @param string $sArrayType - the type of Config array (e.g. 'arr', 'aarr')
     */
    protected static function _addOptionsToArray(
        array $aConfig = [],
        array $aOptions = [],
        string $sArrayType = 'arr'
    ): array {
        $aConfig = (is_array($aConfig) ? $aConfig : []);
        if (is_array($aOptions)) {
            foreach ($aOptions as $mKey => $sOption) {
                if (
                    $sArrayType === 'arr'
                    && !in_array($sOption, $aConfig, true)
                ) {
                    $aConfig[] = $sOption;
                } elseif (
                    $sArrayType === 'aarr'
                    && (
                        !isset($aConfig[$mKey])
                        || ($aConfig !== $sOption)
                    )
                ) {
                    $aConfig[$mKey] = $sOption;
                }
            }
        }

        return $aConfig;
    }

    protected function _varValueWithTypeInfo(string $sVarName, $mVarValue, string $sVarType)
    {
        // if array contains more than one item it can be distinguished from the assoc array we use for type
        // arrays can be recognised
        if ($sVarType !== 'arr' && !($sVarType === 'aarr' && count($mVarValue) > 1)) {
            // default type
            $typeInfoNeeded = true;
            $boolPrefix = strpos($sVarName, 'bl') === 0;

            if ($sVarType === 'str' && !$boolPrefix) {
                $typeInfoNeeded = false;
            }

            if ($sVarType === 'select' && !$boolPrefix) {
                $typeInfoNeeded = false;
            }

            if ($sVarType === 'bool' && $boolPrefix) {
                $typeInfoNeeded = false;
            }

            if (
                $sVarType === 'bool'
                && (
                    $mVarValue === '1'
                    || $mVarValue === ''
                    || $mVarValue === 'true'
                    || $mVarValue === 'false'
                )
            ) {
                $mVarValue = (bool) $mVarValue;
                $typeInfoNeeded = false;
            }

            if ($typeInfoNeeded) {
                $mVarValue = [$sVarType => $mVarValue];
            }
        }

        return $mVarValue;
    }
}
