<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class ToolsMonologLogger
{
    /** Logger with Logger Obj */
    protected static array $_aLogger = [];

    /**
     * get Mono-Logger.
     *
     * @param string $sLogger   - Logger-name
     * @param string $sPath     - Logfile-Path
     * @param string $sLogLevel - Log Level
     *
     * @throws Exception
     */
    public static function getLogger(string $sLogger = '', string $sPath = '', string $sLogLevel = ''): Logger
    {
        $sLogger = $sLogger ?: 'OXID Logger';

        if (!isset(self::$_aLogger[$sLogger])) {
            $sPath = $sPath ?: Registry::getConfig()->getLogsDir() . 'oxideshop.log';
            if (!is_file($sPath)) {
                file_put_contents($sPath, '');
            }

            $sLogLevel = Str::getStr()->strtoupper($sLogLevel);
            $iLogLevel = defined('Logger::' . $sLogLevel) ? constant('Logger::' . $sLogLevel) : Logger::DEBUG;

            self::$_aLogger[$sLogger] = new Logger($sLogger);
            self::$_aLogger[$sLogger]->pushHandler(new StreamHandler(
                $sPath,
                $iLogLevel
            ));
        }

        return self::$_aLogger[$sLogger];
    }
}
