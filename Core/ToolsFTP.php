<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

class ToolsFTP
{
    /**
     * sync Files with a remote FTP.
     *
     * @param string $sRemoteDir Remote Dir
     * @param string $sLocaleDir Locale Dir
     */
    public static function syncFiles(
        string $sHost,
        int $iPort = 21,
        string $sUserName = '',
        string $sPassWord = '',
        string $sRemoteDir = '',
        string $sLocaleDir = '',
        bool $bUseSSL = false,
        bool $bUsePassivMode = true
    ): array {
        $aResult = [];

        // set up basic connection
        $ftpFunc = $bUseSSL ? 'ftp_ssl_connect' : 'ftp_connect';
        $oConnId = $ftpFunc($sHost, $iPort);

        // check to see if server is responding
        if (!$oConnId) {
            ToolsLog::setDebugLogEntry([
                sprintf(
                    'FTP-Server not accessable: %s://%s:%u',
                    $bUseSSL ? 'ftps' : 'ftp',
                    $sHost,
                    $iPort
                ),
            ]);
        }

        // login with username and password
        if (!ftp_login($oConnId, $sUserName, $sPassWord)) {
            ToolsLog::setDebugLogEntry([
                sprintf(
                    'could not Login on FTP-Server: %s@%s',
                    $sUserName,
                    $sHost
                ),
            ]);
        }

        // activate Passive-Mode
        ftp_pasv($oConnId, $bUsePassivMode);

        // get List of new synced files
        $aResult = self::collectFiles($oConnId, $sRemoteDir, $sLocaleDir);

        // close the connection
        ftp_close($oConnId);

        return $aResult;
    }

    /**
     * get the debug Log.
     *
     * @param bool $bTargetTRWToolsLog - should send the Log to the TRWTools
     */
    public static function getDebugLog(bool $bTargetTRWToolsLog = false): array
    {
        return ToolsLog::getDebugLog($bTargetTRWToolsLog, __CLASS__);
    }

    /**
     * Connect to Webservice via cUrl.
     *
     * @param resource $oConnId    FTP-Resource
     * @param string   $sRemoteDir Remote Dir
     * @param string   $sLocaleDir Locale Dir
     */
    protected static function collectFiles(
        $oConnId,
        string $sRemoteDir = '.',
        string $sLocaleDir = ''
    ): array {
        $newFiles = [];

        $aFileList = ftp_nlist($oConnId, $sRemoteDir);
        foreach ($aFileList as $sFile) {
            $res = ftp_size($oConnId, $sFile);
            $local_file = $sLocaleDir . str_replace($sRemoteDir, '', $sFile);
            if ($res !== -1) {
                if (!file_exists($local_file)) {
                    $newFiles[] = $local_file;
                    ftp_get($oConnId, $local_file, $sFile, FTP_BINARY);
                    touch($local_file, ftp_mdtm($oConnId, $sFile));
                }
            } else {
                if (!mkdir($local_file, 0777, true) && !is_dir($local_file)) {
                    ToolsLog::setDebugLogEntry([
                        'could not create folder: ' . $local_file,
                    ]);
                }
                $newFiles += collectFiles($oConnId, $sFile, $sLocaleDir);
            }
        }

        return $newFiles;
    }
}
