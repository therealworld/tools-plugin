<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use ZipArchive;

class ToolsFile
{

    public const FILEAGEFORIMPORTDEFAULT = 360;

    /** array with default excluded files for folderscans */
    protected static array $_aDefaultExcludedFiles = [
        '.',
        '..',
    ];

    /**
     * determines the files in a directory
     * If a filter is defined, then only the filtered files come into the result array
     * A filter can be equipped with an exclude filter.
     *
     * @param string $sPath              scanned path
     * @param string $sFilterRegex       regex for filter the file
     * @param bool   $bObserveFileChange - chould we observe the last change of the file
     */
    public static function getFileNamesFromPath(
        string $sPath = '',
        string $sFilterRegex = '',
        bool $bObserveFileChange = false
    ): array {
        $aResult = [];

        $iFileAgeForImport = self::getFileAgeForImport();

        if ($sPath && is_dir($sPath)) {
            $aEntries = scandir($sPath);
            $aFiles = [];
            foreach ($aEntries as $sEntry) {
                if (
                    in_array($sEntry, self::$_aDefaultExcludedFiles, true)
                    || is_dir($sPath . $sEntry)
                    || (
                        is_file($sPath . $sEntry)
                        && $bObserveFileChange === true
                        && filemtime($sPath . $sEntry) > (time() - $iFileAgeForImport)
                    )
                ) {
                    continue;
                }

                // regex Filter
                if (!empty($sFilterRegex) && (preg_match($sFilterRegex, $sEntry) !== 1)) {
                    continue;
                }

                $aFiles[] = $sEntry;
            }
            if (count($aFiles)) {
                $aResult = $aFiles;
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('found follow file(s): %s', 'TRWTOOLSPLUGIN'),
                        implode(', ', $aFiles)
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__
                );
            }
        } else {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPath
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
        }

        return $aResult;
    }

    /**
     * move files on the server
     * If a filter is defined, then only the filtered files are moved.
     *
     * @param string $sPathSource        Source Path
     * @param string $sPathTarget        Target Path
     * @param string $sFilterRegex       regex for filter the file
     * @param bool   $bObserveFileChange - should we observe the last change of the file
     * @param bool   $bIsArchive         - if true a timestamp would be add
     * @param bool   $bIsCopy            - If true, the file is copied and not renamed
     */
    public static function moveFiles(
        string $sPathSource = '',
        string $sPathTarget = '',
        string $sFilterRegex = '',
        bool $bObserveFileChange = false,
        bool $bIsArchive = false,
        bool $bIsCopy = false
    ): bool {
        if (!$sPathSource) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathSource'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );

            return false;
        }

        if (!$sPathTarget) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathTarget'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );

            return false;
        }

        if (!self::checkPathPermissions($sPathSource, false, true, false, false)) {
            return false;
        }

        if (!self::checkPathPermissions($sPathTarget, false, false, true, false)) {
            return false;
        }

        $sFunctionName = $bIsCopy ? 'copy' : 'rename';

        if ($aFiles = self::getFileNamesFromPath($sPathSource, $sFilterRegex, $bObserveFileChange)) {
            foreach ($aFiles as $sFile) {
                self::moveFile($sFile, $sPathSource, $sPathTarget, $bIsArchive, $bIsCopy);
            }
        }

        return true;
    }

    /**
     * move single file on the server.
     *
     * @param string $sFile       Name of File
     * @param string $sPathSource Source Path
     * @param string $sPathTarget Target Path
     * @param bool   $bIsArchive  - if true a timestamp would be add
     * @param bool   $bIsCopy     - If true, the file is copied and not renamed
     */
    public static function moveFile(
        string $sFile = '',
        string $sPathSource = '',
        string $sPathTarget = '',
        bool $bIsArchive = false,
        bool $bIsCopy = false
    ): bool {
        $sFunctionName = $bIsCopy ? 'copy' : 'rename';

        $sTargetFile = ($bIsArchive ? self::getBackupName($sFile, '_archive_', true) : $sFile);
        if (
            $sFunctionName($sPathSource . $sFile, $sPathTarget . $sTargetFile)
        ) {
            $sResult = 'File moved: %s -> %s';
            if ($bIsArchive) {
                $sResult = 'Backup created: %s -> %s';
            } elseif ($bIsCopy) {
                $sResult = 'File copied: %s -> %s';
            }
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString($sResult, 'TRWTOOLSPLUGIN'),
                    $sFile,
                    $sTargetFile
                ),
                __CLASS__ . ' - ' . __FUNCTION__
            );
        } else {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Failed to move or rename the file: %s -> %s', 'TRWTOOLSPLUGIN'),
                    $sFile,
                    $sTargetFile
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
        }

        return true;
    }

    /**
     * move paths on the server.
     *
     * @param string $sPathSource Source Path
     * @param string $sPathTarget Target Path
     * @param bool   $bIsArchive  - if true a timestamp would be add
     */
    public static function movePath(string $sPathSource = '', string $sPathTarget = '', bool $bIsArchive = false): bool
    {
        if (!$sPathSource) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathSource'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );

            return false;
        }

        if (!$sPathTarget) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathTarget'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );

            return false;
        }

        $sPathTarget = ($bIsArchive ? self::getBackupName($sPathTarget, '_archive_') : $sPathTarget);

        if ($bIsArchive) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Backup created: %s -> %s', 'TRWTOOLSPLUGIN'),
                    "\n&nbsp;&nbsp;" . $sPathSource,
                    "\n&nbsp;&nbsp;" . $sPathTarget
                ),
                __CLASS__ . ' - ' . __FUNCTION__
            );
        }

        if (!self::checkPathPermissions($sPathSource, false, true, true, false)) {
            return false;
        }

        return rename($sPathSource, $sPathTarget);
    }

    /**
     * move and extract a Zipcompressed file.
     *
     * @param string $sSourceName        the name of the sourcefile
     * @param string $sSourceExt         the extension of the sourcefile
     * @param string $sPathSource        the path of the sourcefile
     * @param string $sPathTarget        the targetpath of the extraction
     * @param string $sPathReady         the path for the archive of the extracted files
     * @param bool   $bObserveFileChange - chould we observe the last change of the file
     *
     * @return int Resultcode as Binary-Code: 1 = file are extract,
     *             2 readyfile are moved to archive,
     *             4 = targetpath moved to archive
     */
    public static function moveAndExtractFile(
        string $sSourceName = '',
        string $sSourceExt = 'zip',
        string $sPathSource = '',
        string $sPathTarget = '',
        string $sPathReady = '',
        bool $bObserveFileChange = false
    ): int {
        $iResult = 0;

        $sSourceFile = $sSourceName . '.' . $sSourceExt;
        $sPathExtractTarget = $sPathTarget . $sSourceName;

        if (file_exists($sPathSource . $sSourceFile)) {
            // create a possible backup
            if (
                self::movePath(
                    $sPathExtractTarget,
                    $sPathExtractTarget,
                    true
                )
            ) {
                $iResult |= 4;
            }

            // extract the new file
            if (
                self::extractZipFile(
                    $sPathSource,
                    $sSourceFile,
                    $sPathExtractTarget,
                    $bObserveFileChange
                )
            ) {
                $iResult |= 1;
            }

            // move the extracted file
            if (
                self::moveFiles(
                    $sPathSource,
                    $sPathReady,
                    '/^' . $sSourceFile . '$/',
                    false,
                    true
                )
            ) {
                $iResult |= 2;
            }
        }

        return $iResult;
    }

    /**
     * GZIPs a file on disk (appending .gz to the name).
     *
     * @param string $sFileSource Source File
     * @param string $sPathSource Source Path
     * @param string $sFileTarget optional Target File
     * @param string $sPathTarget optional Target Path
     * @param int    $iLevel      GZIP compression level (default: 9)
     */
    public static function compressGZFile(
        string $sFileSource = '',
        string $sPathSource = '',
        string $sFileTarget = '',
        string $sPathTarget = '',
        int $iLevel = 9
    ): bool {
        $bResult = false;
        $bError = false;

        if (!$sFileTarget) {
            $sFileTarget = $sFileSource;
        }
        if (!$sPathTarget) {
            $sPathTarget = $sPathSource;
        }
        $sFileTarget .= '.gz';

        if (!is_dir($sPathSource)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathSource
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
        }

        if (!is_dir($sPathTarget)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathTarget
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
        }

        if (!is_file($sPathSource . $sFileSource)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('File not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathSource . $sFileSource
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
        }

        if (!$bError) {
            if ($oOut = gzopen($sPathTarget . $sFileTarget, 'wb' . $iLevel)) {
                if ($oIn = fopen($sPathSource . $sFileSource, 'rb')) {
                    while (!feof($oIn)) {
                        gzwrite($oOut, fread($oIn, 1024 * 512));
                    }
                    fclose($oIn);
                    $bResult = true;
                } else {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Could not open file: %s', 'TRWTOOLSPLUGIN'),
                            $sPathSource . $sFileSource
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
                gzclose($oOut);
            } else {
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('Could not open file: %s', 'TRWTOOLSPLUGIN'),
                        $sPathTarget . $sFileTarget
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__,
                    'error'
                );
            }
        }

        return $bResult;
    }

    /**
     * extract a zip-File.
     *
     * @param string $sPathSource        Source Path
     * @param string $sFileName          Filename
     * @param string $sPathTarget        Target Path
     * @param bool   $bObserveFileChange - chould we observe the last change of the file
     */
    public static function extractZipFile(
        string $sPathSource = '',
        string $sFileName = '',
        string $sPathTarget = '',
        bool $bObserveFileChange = false
    ): bool {
        $bResult = false;
        $iFileAgeForImport = self::getFileAgeForImport();
        if (
            is_file($sPathSource . $sFileName)
            && (
                (
                    $bObserveFileChange === true
                    && filemtime($sPathSource . $sFileName) > (time() - $iFileAgeForImport)
                )
                || $bObserveFileChange === false
            )
        ) {
            if ($oZip = new ZipArchive()) {
                try {
                    $oZip->open($sPathSource . $sFileName);
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('extract: %s', 'TRWTOOLSPLUGIN'),
                            $sPathSource . $sFileName
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'info'
                    );

                    if ($sPathTarget && !file_exists($sPathTarget) && !mkdir($sPathTarget) && !is_dir($sPathTarget)) {
                        ToolsLog::setLogEntry(
                            sprintf(
                                ToolsLang::translateString('Cant create path: %s', 'TRWTOOLSPLUGIN'),
                                $sPathTarget
                            ),
                            __CLASS__ . ' - ' . __FUNCTION__,
                            'error'
                        );
                    }

                    $oZip->extractTo($sPathTarget ?: $sPathSource);
                    $oZip->close();
                    $bResult = true;
                } catch (Exception $e) {
                    $sMessage = $e->getMessage();
                    ToolsLog::setLogEntry(
                        $sMessage,
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
            }
        }

        return $bResult;
    }

    /**
     * return a Backupname of a file or a path with timestamp.
     *
     * @param string $sString    - the file- or pathname
     * @param string $sAddString - the part in the Backupfilename between the originalname and the timestamp
     * @param bool   $bIsFile    - is it a file? If true we consider the fileextension
     */
    public static function getBackupName(
        string $sString = '',
        string $sAddString = '_backup_',
        bool $bIsFile = false
    ): string {
        $sBackupName = '';
        if ($sString) {
            $sAddTimeStamp = date('YmdHis');
            if ($bIsFile && ($sPosFileExtension = strrpos($sString, '.')) !== false) {
                $sBackupName .= substr($sString, 0, $sPosFileExtension);
                $sBackupName .= $sAddString;
                $sBackupName .= $sAddTimeStamp;
                $sBackupName .= substr($sString, $sPosFileExtension);
            } else {
                $sBackupName .= $sString;
                $sBackupName .= $sAddString;
                $sBackupName .= $sAddTimeStamp;
            }
        }

        return $sBackupName;
    }

    /**
     * Delete files Recursively.
     *
     * @param string $sPath            - Filepath
     * @param bool   $bDeleteRootPath  - RootPath?
     * @param bool   $bExcludeHtaccess - exclude also htaccess ?
     */
    public static function deleteRecursively(
        string $sPath = '',
        bool $bDeleteRootPath = true,
        bool $bExcludeHtaccess = true
    ): void {
        if (is_dir($sPath)) {
            $aFiles = scandir($sPath);

            $aExcludedFiles = self::$_aDefaultExcludedFiles;
            if ($bExcludeHtaccess) {
                $aExcludedFiles[] = '.htaccess';
            }

            foreach ($aFiles as $sFile) {
                if (!in_array($sFile, $aExcludedFiles, true)) {
                    if (filetype($sPath . $sFile) === 'dir') {
                        self::deleteRecursively($sPath . $sFile . '/');
                    } else {
                        self::deleteFile($sPath . $sFile);
                    }
                }
            }
            if ($bDeleteRootPath) {
                rmdir($sPath);
            }
        }
    }

    /**
     * Removes files from path.
     */
    public static function deleteFilesFromPath(string $sPath = '', ?int $iSeconds = null): void
    {
        if (is_dir($sPath)) {
            $aFiles = glob($sPath . '*');
            if (is_array($aFiles)) {
                // delete all template cache files
                foreach ($aFiles as $sFile) {
                    self::deleteFile($sFile, $iSeconds);
                }
            }
        }
    }

    /**
     * Delete a file if it exists and meets the given age condition.
     *
     * @param string $sFile - The path + filename.
     * @param int|null $iSeconds - The file age threshold in seconds (optional).
     */
    public static function deleteFile(string $sFile = '', ?int $iSeconds = null): void
    {
        if (!$sFile || !is_file($sFile)) {
            return;
        }

        $now = time();
        $fileAge = $now - filemtime($sFile);

        if ($iSeconds === null || $fileAge > $iSeconds) {
            @unlink($sFile);
        }
    }

    /**
     * check a path and create it, if its not exists.
     *
     * @param string $sPath     - The Path + Filename
     * @param bool   $bRelative - is the path relative to the shoproot?
     */
    public static function createPath(string $sPath = '', bool $bRelative = true): bool
    {
        $bResult = false;
        if ($sPath) {
            $sFullPath = ($bRelative ? Registry::getConfig()->getConfigParam('sShopDir') : '');
            $sFullPath .= $sPath;
            if (file_exists($sFullPath) && is_dir($sFullPath)) {
                $bResult = true;
            } else {
                if (!$bResult = @mkdir($sFullPath, 0755, true)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Cant create path: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__
                    );
                }
            }
        }

        return $bResult;
    }

    /**
     * check the permissions of a path.
     *
     * @param string $sPath       - The Path
     * @param bool   $bRelative   - is the path relative to the shoproot?
     * @param bool   $bReadable   - check readable
     * @param bool   $bWritable   - check writable
     * @param bool   $bCreatePath - create a path if it doesnt exists?
     */
    public static function checkPathPermissions(
        string $sPath,
        bool $bRelative = true,
        bool $bReadable = true,
        bool $bWritable = true,
        bool $bCreatePath = true
    ): bool {
        $bResult = true;
        if ($sPath) {
            $sFullPath = ($bRelative ? Registry::getConfig()->getConfigParam('sShopDir') : '');
            $sFullPath .= $sPath;
            if (!$bPathExists = (file_exists($sFullPath) && is_dir($sFullPath))) {
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                        $sFullPath
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__,
                    'error'
                );
                if ($bCreatePath) {
                    $bPathExists = self::createPath($sPath, $bRelative);
                }
            }

            if ($bPathExists) {
                if ($bReadable && !is_readable($sFullPath)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Path not readable: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                    $bResult = false;
                }
                if ($bWritable && !is_writable($sFullPath)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Pfad not writable: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                    $bResult = false;
                }
            }
        } else {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    $sPath
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bResult = false;
        }

        return $bResult;
    }

    /**
     * delete all Images of Article.
     *
     * @param Article $oArticle given article
     * @param int - $iMaxImage - The max count of masterpictures
     *
     * @return null
     */
    public static function deleteAllImagesOfArticle(Article $oArticle, int $iMaxImage): void
    {
        $oPicHandler = Registry::getPictureHandler();
        if (!$oArticle->isDerived()) {
            // Icon
            if ($oArticle->getFieldData('oxicon')) {
                $oPicHandler->deleteMainIcon($oArticle);
                $oArticle->oxarticles__oxicon = new Field();
            }

            // Thumbnail
            if ($oArticle->getFieldData('oxthumb')) {
                $oPicHandler->deleteThumbnail($oArticle);
                $oArticle->oxarticles__oxthumb = new Field();
            }

            // MasterPictures
            if ($iMaxImage) {
                for ($iIndex = 1; $iIndex <= $iMaxImage; $iIndex++) {
                    if ($oArticle->getFieldData('oxpic' . $iIndex)) {
                        $oPicHandler->deleteArticleMasterPicture($oArticle, $iIndex);
                        $oArticle->{'oxarticles__oxpic' . $iIndex} = new Field();
                    }
                    if ($oArticle->getFieldData('oxzoom' . $iIndex)) {
                        $oArticle->{'oxarticles__oxzoom' . $iIndex} = new Field();
                    }
                }
            }
        }
    }

    /**
     * create a relative Path from one Url to another.
     *
     * @param string $sFrom - The source Url
     * @param string $sTo   - The target Url
     */
    public static function getRelativePath(string $sFrom, string $sTo): string
    {
        // some compatibility fixes for Windows paths
        $sFrom = is_dir($sFrom) ? rtrim($sFrom, '\/') . '/' : $sFrom;
        $sFrom = str_replace('\\', '/', $sFrom);
        $sTo = is_dir($sTo) ? rtrim($sTo, '\/') . '/' : $sTo;
        $sTo = str_replace('\\', '/', $sTo);

        $aFrom = explode('/', $sFrom);
        $aTo = explode('/', $sTo);
        $aRelativePath = $aTo;

        foreach ($aFrom as $iDepth => $sDir) {
            // find first non-matching dir
            if ($sDir === $aTo[$iDepth]) {
                // ignore this directory
                array_shift($aRelativePath);
            } else {
                // get number of remaining dirs to $aFrom
                $iRemaining = count($aFrom) - $iDepth;
                if ($iRemaining > 1) {
                    // add traversals up to first matching dir
                    $iPadLength = (count($aRelativePath) + $iRemaining - 1) * -1;
                    $aRelativePath = array_pad($aRelativePath, $iPadLength, '..');

                    break;
                }

                if ($iRemaining === 1) {
                    $aRelativePath[0] = './' . $aRelativePath[0];
                }
            }
        }

        return implode('/', $aRelativePath);
    }

    /**
     * clean up a file for specific string
     *
     * @param string $sFilePath - File
     * @param string $sSearch   - SearchTermm
     */
    public static function removeStringFromFile(string $sFilePath, string $sSearch): bool
    {
        $bResult = false;
        if (file_exists($sFilePath)) {
            $sContent = file_get_contents($sFilePath);
            if ($sContent !== false) {
                $sNewContent = str_replace($sSearch, '', $sContent);
                $bResult = (bool)file_put_contents($sFilePath, $sNewContent);
            }
        }
        return $bResult;
    }

    private static function getFileAgeForImport(): int
    {
        $iFileAgeForImport = self::FILEAGEFORIMPORTDEFAULT;

        if (ToolsConfig::getModuleExists('trwtools')) {
            $iFileAgeForImport = (int) Registry::getConfig()->getConfigParam('iTRWToolsFileAgeForImport');
        }

        return $iFileAgeForImport;
    }
}
