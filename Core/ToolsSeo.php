<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class ToolsSeo
{
    /**
     * create a static Url.
     *
     * @param string $sStdUrl - the standard url e.g. index.php?cl=xyz
     * @param array  $aSeoUrl - array with all static seo-urls, keys = langabbr e.g. ['de' => 'xyz/', 'en' => 'en/xyz/']
     *
     * @throws Exception
     */
    public static function setStaticUrl(string $sStdUrl, array $aSeoUrl): string
    {
        $sResult = '';
        if (!self::_getIdForStaticUrl($sStdUrl)) {
            $aStaticUrl = [
                'oxseo__oxstdurl' => $sStdUrl,
            ];

            $oLang = Registry::getLang();
            foreach ($oLang->getLanguageIds() as $iLang => $sLangAbbr) {
                if (array_key_exists($sLangAbbr, $aSeoUrl)) {
                    $aStaticUrl['oxseo__oxseourl'][$iLang] = $aSeoUrl[$sLangAbbr];
                }
            }

            if (isset($aStaticUrl['oxseo__oxseourl'])) {
                $sResult = Registry::getSeoEncoder()->encodeStaticUrls(
                    $aStaticUrl,
                    Registry::getConfig()->getShopId(),
                    $oLang->getEditLanguage()
                );
            }
        }

        return $sResult;
    }

    /**
     * delete a static Url.
     *
     * @param string $sStdUrl - the standard url e.g. index.php?cl=xyz
     *
     * @return bool
     */
    public static function deleteStaticUrl(string $sStdUrl): void
    {
        $iShopId = Registry::getConfig()->getShopId();

        foreach (Registry::getLang()->getLanguageIds() as $iLang => $sLangAbbr) {
            $sObjectId = self::_getIdForStaticUrl(
                $sStdUrl,
                $iLang,
                $iShopId
            );

            Registry::getSeoEncoder()->deleteSeoEntry(
                $sObjectId,
                $iShopId,
                $iLang,
                'static'
            );
        }
    }

    /**
     * update the Seo Meta Data.
     *
     * @param string   $sOxId   Descriptions
     * @param int      $iLang   active language (optional). default null
     * @param null|int $iShopId active shop id (optional). default null
     *
     * @throws DatabaseConnectionException
     */
    public static function updateSeoMeta(
        string $sOxId,
        string $sKeywords,
        string $sDescription,
        int $iLang = 0,
        ?int $iShopId = null
    ): bool {
        $oStr = Str::getStr();
        $oDb = DatabaseProvider::getDb();

        if (is_null($iShopId)) {
            $iShopId = Registry::getConfig()->getShopId();
        }

        $iLang = $iLang ?: (int) Registry::getLang()->getBaseLanguage();

        if ($sKeywords !== '') {
            $sKeywords = $oStr->htmlspecialchars(
                Registry::getSeoEncoder()->encodeString($oStr->strip_tags($sKeywords), false, $iLang)
            );
        }

        if ($sDescription !== '') {
            $sDescription = $oStr->htmlspecialchars(
                $oStr->strip_tags($sDescription)
            );
        }

        $sObj2SeoDataTable = 'oxobject2seodata';
        $sSeoQuery = "insert into {$sObj2SeoDataTable}
            (`oxobjectid`, `oxshopid`, `oxlang`, `oxkeywords`, `oxdescription`)
            values
            (
                " . $oDb->quote($sOxId) . ',
                ' . $iShopId . ',
                ' . $iLang . ',
                ' . $oDb->quote($sKeywords) . ',
                ' . $oDb->quote($sDescription) . '
            )
            on duplicate key update
            oxkeywords = ' . $oDb->quote($sKeywords) . ', oxdescription = ' . $oDb->quote($sDescription);

        return ToolsDB::execute($sSeoQuery) !== 0;
    }

    /**
     * Returns static url for passed standard link (if available).
     *
     * @param string   $sStdUrl standard Url
     * @param int      $iLang   active language (optional). default null
     * @param null|int $iShopId active shop id (optional). default null
     */
    protected static function _getIdForStaticUrl(string $sStdUrl, int $iLang = 0, ?int $iShopId = null): string
    {
        $aParams = [
            'oxstdurl' => $sStdUrl,
        ];

        if (!is_null($iShopId)) {
            $aParams['oxshopid'] = $iShopId;
        }
        if (!is_null($iLang)) {
            $aParams['oxlang'] = $iLang;
        }

        $result = ToolsDB::getAnyId(
            'oxseo',
            $aParams,
            'oxobjectid'
        );

        return $result ? $result : '';
    }
}
