<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Core;

use ErrorException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use SoapClient;
use SoapFault;

class ToolsApi
{
    /** array with possible Method Types */
    protected static array $_aPossibleMethodTypes = ['POST', 'GET', 'DELETE', 'PATCH'];

    /** the default cUrl Options */
    protected static array $_aCUrlDefaults = [
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_TIMEOUT       => 5,
    ];

    /**
     * Connect to Webservice via Guzzle.
     *
     * @param string $sAPIUrl      - The Url of the extern API
     * @param string $sMethod      - The Method for call (e.g. POST, GET, DELETE)
     * @param mixed  $mRequestData - Request-Parameters as Array or JSON-String
     * @param array  $aLoginData   - Array with authentication Data - we have the following possibilities
     *                             - Basic User/Password -> ['basic' => ['user' => 'xxx', 'password' => 'yyy']]
     *                             - Bearer Token        -> ['bearertoken' => 'xxx']
     *                             - Base64 Token        -> ['base64token' => 'xxx']
     *                             - a token without exact authorization method description, only "authorization"
     * @param string $sTargetFile  - if we need to save the result in a file, the filename (+ Path)
     * @param array  $aHeaders     - Array with Headers
     * @param bool   $bDebug       - DebugModus?
     */
    public static function executeExternApiGuzzle(
        string $sAPIUrl = '',
        string $sMethod = 'POST',
        $mRequestData = null,
        array $aLoginData = [],
        string $sTargetFile = '',
        array $aHeaders = [],
        bool $bDebug = false
    ): array {
        $aResult = [
            'result'       => false,
            'responseCode' => null,
        ];

        $sAuth = self::_convertLoginToHeaderAuth($aLoginData);

        if ($sAuth) {
            $aHeaders += [
                'Authorization' => $sAuth,
            ];
        }

        $aRequestOptions = [];
        $aRequestOptionsDebug = [];

        if (isset($mRequestData[RequestOptions::MULTIPART])) {
            foreach ($mRequestData[RequestOptions::MULTIPART] as $aMultiPart) {
                $aRequestOptionsDebug[RequestOptions::MULTIPART][] = $aMultiPart;
                if (isset($aMultiPart['isResource'])) {
                    $aMultiPart['contents'] = fopen($aMultiPart['contents'], 'rb');
                    unset($aMultiPart['isResource']);
                }
                $aRequestOptions[RequestOptions::MULTIPART][] = $aMultiPart;
            }
        } else {
            $sDataType = $sMethod === 'GET' ? 'query' : 'body';
            if (is_array($mRequestData)) {
                $mRequestData = http_build_query($mRequestData);
                if ($sMethod === 'GET') {
                    // fix numeric arrays in GET-URLs: remove counter, only brackets
                    $mRequestData = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $mRequestData);
                }
            }
            $aAddRequestOptions = [
                'headers'     => $aHeaders,
                'http_errors' => false,
                $sDataType    => $mRequestData,
            ];
            $aRequestOptions += $aAddRequestOptions;
            $aRequestOptionsDebug += $aAddRequestOptions;
        }

        // files will be stored directly in the file
        if ($sTargetFile) {
            $aRequestOptions['sink'] = $sTargetFile;
            $aRequestOptionsDebug['sink'] = $sTargetFile;
        }

        $responseCode = null;
        $responseBody = '';
        $logResult = '';

        try {
            $client = new Client();
            $response = $client->request(
                $sMethod,
                $sAPIUrl,
                $aRequestOptions
            );

            $responseCode = $response->getStatusCode();
            $responseBody = $sTargetFile ?: $response->getBody()->getContents();

            $aResult = [
                'result'       => $responseBody,
                'responseCode' => $responseCode,
            ];
        } catch (GuzzleException $e) {
            $bDebug = true;
            $logResult = $e->getMessage();
        }

        // possible debugging
        if ($bDebug) {
            $logResult = !empty($logResult) ?: htmlentities($responseBody);

            $aDebug = [
                'ApiGuzzleUrl'        => $sAPIUrl,
                'ApiGuzzleOptions'    => $aRequestOptionsDebug,
                'ApiGuzzleHttpStatus' => $responseCode,
                'ApiGuzzleResult'     => $logResult,
            ];

            ToolsLog::setDebugLogEntry($aDebug);
        }

        return $aResult;
    }

    /**
     * Connect to Webservice via cUrl.
     *
     * @param string $sAPIUrl                - The Url of the extern API
     * @param mixed  $mRequestData           - Request-Parameters as Array or JSON-String
     * @param array  $aLoginData             - Array with authentication Data - we have the following possibilities
     *                                       - Basic User/Password -> ['basic' => [
     *                                       'user' => 'xxx',
     *                                       'password' => 'yyy'
     *                                       ]]
     *                                       - Bearer Token        -> ['bearertoken' => 'xxx']
     *                                       - Base64 Token        -> ['base64token' => 'xxx']
     *                                       - a token without exact authorization method description,
     *                                       only "authorization"
     * @param array  $aOverwriteCUrlDefaults - POST, GET (default)
     * @param string $sTargetFile            - if we need to save the result in a file, the filename (+ Path)
     * @param array  $aCurlHeaders           - Array with optional cUrl Headers
     * @param bool   $bDebug                 - DebugModus?
     */
    public static function executeExternApiCurl(
        string $sAPIUrl = '',
        $mRequestData = null,
        array $aLoginData = [],
        array $aOverwriteCUrlDefaults = [],
        string $sTargetFile = '',
        array $aCurlHeaders = [],
        bool $bDebug = false
    ): array {
        // cUrl Basics
        $aCurlOptions = self::$_aCUrlDefaults;

        // overwrite cUrl Defaults
        if (is_array($aOverwriteCUrlDefaults) && count($aOverwriteCUrlDefaults)) {
            foreach ($aOverwriteCUrlDefaults as $iCUrlKey => $mCUrlValue) {
                // check RequestType
                if ($iCUrlKey === CURLOPT_CUSTOMREQUEST) {
                    $mCUrlValue = (
                        in_array($mCUrlValue, self::$_aPossibleMethodTypes, true) ?
                        $mCUrlValue :
                        self::$_aCUrlDefaults[CURLOPT_CUSTOMREQUEST]
                    );
                }
                $aCurlOptions[$iCUrlKey] = $mCUrlValue;
            }
        }

        // possible Login
        $sAuth = self::_convertLoginToHeaderAuth($aLoginData);
        if ($sAuth) {
            $aCurlHeaders[] = $sAuth;
        }

        // cUrl Headers
        if (count($aCurlHeaders)) {
            $aCurlOptions[CURLOPT_HTTPHEADER] = $aCurlHeaders;
        }

        // cUrl RequestData
        if ($aCurlOptions[CURLOPT_CUSTOMREQUEST] === 'POST') {
            $aCurlOptions[CURLOPT_POST] = true;
            $aCurlOptions[CURLOPT_POSTFIELDS] = $mRequestData;
        } else {
            if (is_array($mRequestData) && count($mRequestData)) {
                $mRequestData = http_build_query($mRequestData);
                $sAPIUrl .= '?' . $mRequestData;
            }
        }

        // If defined a target file, we wrote the result in the file
        // open possible file Handle
        $oFileHandle = null;
        if (is_string($sTargetFile) && !empty($sTargetFile)) {
            $oFileHandle = fopen($sTargetFile, 'wb');
            $aCurlOptions[CURLOPT_FILE] = $oFileHandle;
            if (!ini_get('open_basedir')) {
                $aCurlOptions[CURLOPT_FOLLOWLOCATION] = true;
            }
        } else {
            $aCurlOptions[CURLOPT_RETURNTRANSFER] = true;
        }

        // Api-Url
        $aCurlOptions[CURLOPT_URL] = $sAPIUrl;

        // Run the cUrl
        $oCurlHandle = curl_init();

        curl_setopt_array($oCurlHandle, $aCurlOptions);

        $mResult = curl_exec($oCurlHandle);

        $responseCode = (int) curl_getinfo($oCurlHandle, CURLINFO_HTTP_CODE);
        $responseBody = $sTargetFile ?: $mResult;

        $aResult = [
            'result'       => $responseBody,
            'responseCode' => $responseCode,
        ];

        // possible debugging
        if ($bDebug) {
            $aDebug = [
                'ApiCurlUrl'        => $sAPIUrl,
                'ApiCurlOptions'    => $aCurlOptions,
                'ApiCurlHttpStatus' => $responseCode,
                'ApiCurlResult'     => (empty($sTargetFile) ? json_encode($mResult) : $sTargetFile),
            ];
            if ($responseCode !== 200) {
                $aDebug['ApiCurlInfo'] = curl_getinfo($oCurlHandle);
            }

            ToolsLog::setDebugLogEntry($aDebug);
        }

        // close possible file Handle
        if (
            $oFileHandle &&
            !empty($sTargetFile) &&
            file_exists($sTargetFile)

        ) {
            fclose($oFileHandle);
        }

        curl_close($oCurlHandle);

        return $aResult;
    }

    /**
     * Connect to Webservice via simple file_get_contents.
     *
     * @param string $sAPIUrl      - The Url of the extern API
     * @param array  $aRequestData - Array with Request-Parameters
     * @param array  $aLoginData   - Array with authentication Data - for basic authentication
     *                             - Basic User/Password -> ['basic' => ['user' => 'xxx', 'password' => 'yyy']]
     *                             - Bearer Token        -> ['bearertoken' => 'xxx']
     *                             - Base64 Token        -> ['base64token' => 'xxx']
     *                             - a token without exact authorization method description, only "authorization"
     * @param string $sTargetFile  - if we need to save the result in a file, the filename (+ Path)
     * @param bool   $bDebug       - DebugModus?
     * @throws ErrorException
     */
    public static function executeExternApiFileGetContents(
        string $sAPIUrl = '',
        array $aRequestData = [],
        array $aLoginData = [],
        string $sTargetFile = '',
        bool $bDebug = false
    ): ?string {
        $mResult = null;

        if ($sAPIUrl) {
            if (is_array($aRequestData) && count($aRequestData)) {
                $sRequestData = http_build_query($aRequestData);
                $sAPIUrl .= '?' . $sRequestData;
            }

            if ($bDebug) {
                ToolsLog::setDebugLogEntry([
                    'ApiFileGetContentsUrl'         => $sAPIUrl,
                    'ApiFileGetContentsLoginData'   => $aLoginData,
                    'ApiFileGetContentsRequestData' => $aRequestData,
                ]);
            }

            $aContextOptions = [];

            // possible Login
            $sAuth = self::_convertLoginToHeaderAuth($aLoginData);
            if ($sAuth) {
                $aContext['http'] = [
                    'method' => 'GET',
                    'header' => 'Authorization: ' . $sAuth,
                ];
            }

            $aResult = self::_fileGetContentsWithOptions($sAPIUrl, $aContextOptions);

            // try it with some SSL ContextOptions
            if (isset($aResult['error'])) {
                if (stripos($sAPIUrl, 'https') !== false) {
                    if ($bDebug) {
                        ToolsLog::setDebugLogEntry([
                            'ApiErrorFileGetContentsSSL' => sprintf(
                                ToolsLang::translateString(
                                    'API FileGetContents has Problems with SSL in %s (%s), ' .
                                    'second try without peer verify',
                                    'TRWTOOLSPLUGIN'
                                ),
                                $sAPIUrl,
                                $aResult['error']
                            ),
                        ]);
                    }

                    $aContextOptions['ssl'] = [
                        'verify_peer'      => false,
                        'verify_peer_name' => false,
                    ];
                    $aResult = self::_fileGetContentsWithOptions($sAPIUrl, $aContextOptions);

                    if (
                        isset($aResult['error']) &&
                        $bDebug
                    ) {
                        ToolsLog::setDebugLogEntry([
                            'ApiErrorFileGetContentsPeer' => sprintf(
                                ToolsLang::translateString(
                                    'API FileGetContents has Problems with SSL and peer verify in %s (%s)',
                                    'TRWTOOLSPLUGIN'
                                ),
                                $sAPIUrl,
                                $aResult['error']
                            ),
                        ]);
                    }
                }
            }

            if (isset($aResult['result']) && !isset($aResult['error'])) {
                $mResult = $aResult['result'];
            }

            // possible debugging
            if ($bDebug && !$sTargetFile) {
                ToolsLog::setDebugLogEntry([
                    'ApiErrorFileGetContentsResult' => $mResult,
                ]);
            }

            if ($sTargetFile) {
                file_put_contents($sTargetFile, $mResult);
                $mResult = $sTargetFile;
            }
        }

        return $mResult;
    }

    /**
     * Connect to Webservice via SOAP.
     *
     * @param string $sAPIUrl      - The Url of the extern SOAP API
     * @param string $sAPIMethod   - The Method of the extern SOAP API
     * @param array  $aAPIOptions  - Array with options for initialize the extern SOAP API
     * @param array  $aRequestData - Array with Request-Parameters
     * @param bool   $bWSDLMode    - Call in WSDLMode?
     * @param bool   $bDebug       - DebugModus?
     */
    public static function executeExternApiSoap(
        string $sAPIUrl = '',
        string $sAPIMethod = '',
        array $aAPIOptions = [],
        array $aRequestData = [],
        bool $bWSDLMode = false,
        bool $bDebug = false
    ): ?object {
        $mResult = null;

        if ($sAPIUrl && $sAPIMethod && is_array($aRequestData)) {
            if ($bDebug) {
                ToolsLog::setDebugLogEntry([
                    'ApiSoapRequestData' => $aRequestData,
                ]);
            }

            $aClientOptions = [
                'trace'     => 1,
                'exception' => 0,
            ];
            if (is_array($aAPIOptions) && count($aAPIOptions)) {
                $aClientOptions = array_merge($aClientOptions, $aAPIOptions);
            }

            $oSoapClient = new SoapClient(
                $sAPIUrl,
                $aClientOptions
            );

            try {
                if ($bWSDLMode) {
                    $mResult = $oSoapClient->{$sAPIMethod}($aRequestData);
                } else {
                    $mResult = $oSoapClient->__soapCall($sAPIMethod, $aRequestData);
                }
            } catch (SoapFault $e) {
                // possible debugging
                if ($bDebug) {
                    ToolsLog::setDebugLogEntry([
                        'ApiSoapRequest'        => $oSoapClient->__getLastRequest(),
                        'ApiSoapResponseheader' => $oSoapClient->__getLastResponseHeaders(),
                        'ApiSoapResponse'       => $oSoapClient->__getLastResponse(),
                        'ApiSoapErrormessage'   => $e->getMessage(),
                    ]);
                }
            }
        }

        return (object) $mResult;
    }

    /**
     * get the debug Log.
     *
     * @param bool $bTargetTRWToolsLog - should send the Log to the TRWTools
     * @throws JsonException
     */
    public static function getDebugLog(bool $bTargetTRWToolsLog = false): array
    {
        return ToolsLog::getDebugLog($bTargetTRWToolsLog, __CLASS__);
    }

    /**
     * Connect to Webservice via simple file_get_contents.
     *
     * @param string     $sAPIUrl  - The Url of the extern API
     * @param null|array $aContext - Context-Options for file_get_contents
     * @throws ErrorException
     */
    private static function _fileGetContentsWithOptions(string $sAPIUrl, array $aContext = null): array
    {
        $aResult = [];

        $sContext = null;
        if (is_array($aContext) && count($aContext)) {
            $sContext = stream_context_create($aContext);
        }

        // file_get_contents can trigger WARNINGS as a valid result,
        // We also want to intercept the warning and give it back as an error
        set_error_handler(
            static function ($severity, $message) {
                throw new ErrorException($message, $severity, $severity);
            },
            E_WARNING
        );

        try {
            $aResult['result'] = file_get_contents($sAPIUrl, false, $sContext);
        } catch (Exception $e) {
            $aResult['error'] = $e->getMessage();
        }
        restore_error_handler();

        return $aResult;
    }

    /**
     * Connect the Login-Data to Authorize-Headers.
     */
    private static function _convertLoginToHeaderAuth(array $aLoginData = []): string
    {
        $sAuth = '';
        // Basic Authorization
        if (
            isset($aLoginData['basic']['user'], $aLoginData['basic']['password'])
        ) {
            $sAuth = 'Basic ' . base64_encode(
                $aLoginData['basic']['user'] . ':' . $aLoginData['basic']['password']
            );
        } elseif (isset($aLoginData['bearertoken'])) {
            // possible Bearer Token
            $sAuth = 'Bearer ' . $aLoginData['bearertoken'];
        } elseif (isset($aLoginData['base64token'])) {
            // possible Base64 Token without exact authorization method description
            $sAuth = $aLoginData['base64token'];
        }

        return $sAuth;
    }
}
