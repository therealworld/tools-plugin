<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Traits;

/**
 * Convenience trait to work with DataGetter.
 */
trait DataGetter
{
    public function getTRWStringData(string $key): string
    {
        /** @var null|string $value */
        $value = (string) $this->getFieldData($key);

        return $value;
    }

    public function getTRWFloatData(string $key): float
    {
        /** @var null|float $value */
        $value = (float) $this->getFieldData($key);

        return $value;
    }

    public function getTRWIntData(string $key): int
    {
        /** @var null|int $value */
        $value = (int) $this->getFieldData($key);

        return $value;
    }

    public function getTRWArrayData(string $key): array
    {
        /** @var null|array $value */
        $value = $this->getFieldData($key);

        return is_array($value) ? $value : [];
    }

    public function isTRWArrayData(string $key): bool
    {
        /** @var null|array $value */
        $value = $this->getFieldData($key);

        return is_array($value);
    }

    public function getTRWBoolData(string $key): bool
    {
        /** @var null|string $value */
        $value = $this->getFieldData($key);

        return isset($value) && $value === '1';
    }

    public function getTRWRawStringData(string $key): string
    {
        /** @var null|string $value */
        $value = $this->getRawFieldData($key);

        return $value ?? '';
    }
}
