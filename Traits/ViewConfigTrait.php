<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Traits;

use Detection\MobileDetect;
use oxFileException;
use OxidEsales\Eshop\Core\Registry;
use ReflectionClass;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * View config data access class. Keeps most
 * of getters needed for formatting various urls,
 * config parameters, session information etc.
 */
trait ViewConfigTrait
{
    /**
     * is this a "Flow"-Theme Compatible Theme?
     *
     * @param bool
     */
    protected $_bIsFlowCompatibleTheme;

    /**
     * is this a "Wave"-Theme Compatible Theme?
     *
     * @param bool
     */
    protected $_bIsWaveCompatibleTheme;

    /**
     * is this a "Moga"-Theme Compatible Theme?
     *
     * @param bool
     */
    protected $_bIsMogaCompatibleTheme;

    /**
     * Template variable getter. Direct Access to the config.
     *
     * @param mixed $sParamName
     *
     * @return mixed
     */
    public function getConfigParam($sParamName)
    {
        return Registry::getConfig()->getConfigParam($sParamName);
    }

    /**
     * Template variable getter. Direct Access to the config.
     *
     * @param mixed $sParamName
     * @param mixed $bIsAarr
     *
     * @return string
     */
    public function getConfigParamAsString($sParamName, $bIsAarr = true)
    {
        $sResult = '';

        $mValue = $this->getConfigParam($sParamName);

        if ($bIsAarr) {
            foreach ($mValue as $sKey => $sValue) {
                $aResult[] = $sKey . ' => ' . $sValue;
            }
        } else {
            $aResult = $mValue;
        }

        if (is_array($aResult)) {
            $sResult = implode("\n", $aResult);
        }

        return $sResult;
    }

    /**
     * Template variable getter. Direct Access to the Request.
     *
     * @param mixed $sParamName
     *
     * @return mixed
     */
    public function getRequestParameter($sParamName)
    {
        return Registry::getConfig()->getRequestParameter($sParamName);
    }

    /**
     * set Locale via Template.
     */
    public function setLocale()
    {
        ToolsConfig::setLocale();
    }

    /**
     * Get the current shop view.
     *
     * @return string
     */
    public function getCurrentView()
    {
        return Registry::getConfig()->getTopActiveView()->getClassKey();
    }

    /**
     * Get the current shop FrontendController.
     *
     * @return string
     */
    public function getCurrentFrontendController()
    {
        return Registry::getControllerClassNameResolver()->getClassNameById(
            $this->getCurrentView()
        );
    }

    /**
     * Get the current shop FrontendController without namespaces.
     *
     * @return string
     */
    public function getCurrentFrontendControllerShort()
    {
        return (new ReflectionClass($this->getCurrentFrontendController()))->getShortName();
    }

    /**
     * Template variable getter. Return if a Module exsists in a needed Version.
     *
     * @param string sModule - Modulename
     * @param string $sVersion - Version
     * @param mixed  $sModule
     *
     * @return string
     */
    public function getModuleExists($sModule = '', $sVersion = '')
    {
        return ToolsConfig::getModuleExists($sModule, $sVersion);
    }

    /**
     * return url to the requested module file.
     *
     * @param string $sModule module name (directory name in modules dir)
     * @param string $sFile   file name to lookup
     *
     * @return string
     *
     * @throws oxFileException
     */
    public function getModuleUrl($sModule, $sFile = '')
    {
        $sUrl = parent::getModuleUrl($sModule, $sFile);
        if (is_file($this->getModulePath($sModule, $sFile))) {
            $aParts = explode('?', $sUrl);
            if (!isset($aParts[1])) {
                $sUrl .= '?' . filemtime($this->getModulePath($sModule, $sFile));
            }
        }

        return $sUrl;
    }

    /**
     * Return a specific Model, try to load.
     *
     * @param string sModelName - Name of the Model
     * @param string $sId        - possible Id of the Model
     * @param mixed  $sModelName
     *
     * @return mixed (object, boolean)
     */
    public function getModel($sModelName = '', $sId = '')
    {
        $oResult = false;
        if ($oObj = oxNew($sModelName)) {
            $oResult = $oObj;
            if ($sId) {
                $oResult->load($sId);
            }
        }

        return $oResult;
    }

    /**
     * Template variable getter. Get OXID id or others from given table.
     *
     * @param string - $sTable - The table where i found the sObject
     * @param string - $sSearchColName - the column-name for search
     * @param string - $sSearchColValue - the column-value for search
     * @param string - $sResultColName - the column-name of result
     *
     * @return mixed - normally string
     */
    public function getAnyId($sTable, $sSearchColName, $sSearchColValue, $sResultColName)
    {
        return ToolsDB::getAnyId($sTable, [$sSearchColName => $sSearchColValue], $sResultColName);
    }

    /**
     * Template variable getter. Returns country id.
     *
     * @return string
     */
    public function getHomeCountryId()
    {
        if ($this->_sHomeCountryId === null) {
            $this->_sHomeCountryId = false;
            $aHomeCountry = Registry::getConfig()->getConfigParam('aHomeCountry');
            if (is_array($aHomeCountry)) {
                $this->_sHomeCountryId = current($aHomeCountry);
            }
        }

        return $this->_sHomeCountryId;
    }

    /**
     * Template variable getter. Returns Request Uri.
     *
     * @return string
     */
    public function getRequestUri()
    {
        return \OxidEsales\Eshop\Core\Registry::getUtilsServer()->getServerVar('REQUEST_URI');
    }

    /**
     * Template variable getter. Check if is a Flow Theme Compatible Theme.
     *
     * @return bool
     */
    public function isFlowCompatibleTheme()
    {
        if (is_null($this->_bIsFlowCompatibleTheme)) {
            $this->_bIsFlowCompatibleTheme = $this->_isCompatibleTheme('flow');
        }

        return $this->_bIsFlowCompatibleTheme;
    }

    /**
     * Template variable getter. Check if is a Wave Theme Compatible Theme.
     *
     * @return bool
     */
    public function isWaveCompatibleTheme()
    {
        if (is_null($this->_bIsWaveCompatibleTheme)) {
            $this->_bIsWaveCompatibleTheme = $this->_isCompatibleTheme('wave');
        }

        return $this->_bIsWaveCompatibleTheme;
    }

    /**
     * Template variable getter. Check if is a Moga Theme Compatible Theme.
     *
     * @return bool
     */
    public function isMogaCompatibleTheme()
    {
        if (is_null($this->_bIsMogaCompatibleTheme)) {
            $this->_bIsMogaCompatibleTheme = $this->_isCompatibleTheme('moga');
        }

        return $this->_bIsMogaCompatibleTheme;
    }

    /**
     * Template variable getter. Check if is a Mobile Device.
     *
     * @return bool
     */
    public function isMobileDevice()
    {
        $detect = new MobileDetect();

        return $detect->isMobile();
    }

    /**
     * Template variable getter. Check if is a ??? Theme Compatible Theme.
     *
     * @param null|mixed $sTheme
     *
     * @return bool
     */
    protected function _isCompatibleTheme($sTheme = null)
    {
        $bResult = false;
        if ($sTheme) {
            $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
            $oTheme->load($oTheme->getActiveThemeId());
            // check active theme or parent theme
            if (
                $oTheme->getActiveThemeId() == $sTheme
                || $oTheme->getInfo('parentTheme') == $sTheme
            ) {
                $bResult = true;
            }
        }

        return $bResult;
    }
}
