<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Traits;

use JsonException;

/**
 * Convenience trait to work with JSON-Data
 */
trait Json
{
    protected function getTRWJsonPostData(): string
    {
        $result = file_get_contents('php://input');
        return $result ?: '';
    }

    protected function convertTRWJsonToArray(string $json): array
    {
        try {
            $result = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            if (!is_array($result)) {
                throw new JsonException();
            }
        } catch (JsonException $exception) {
            $result = [];
        }
        return $result;
    }

    protected function convertTRWArrayToJson(array $data): string
    {
        try {
            $result = json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
        } catch (JsonException $exception) {
            $result = '';
        }
        return $result;
    }
}