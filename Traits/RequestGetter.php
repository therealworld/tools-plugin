<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsPlugin\Traits;

use OxidEsales\Eshop\Core\Registry;

trait RequestGetter
{
    public function getTrwStringRequestEscapedData(string $key, string $default = ''): string
    {
        $request = Registry::getRequest();
        $value = $request->getRequestEscapedParameter($key, $default);
        return is_string($value) ? $value : $default;
    }

    public function getTrwStringRequestData(string $key, string $default = ''): string
    {
        $request = Registry::getRequest();
        $value = $request->getRequestParameter($key, $default);
        return is_string($value) ? $value : $default;
    }

    public function getTrwFloatRequestData(string $key, float $default = 0.0): float
    {
        $request = Registry::getRequest();
        /** @var string $oxidDefault */
        $oxidDefault = $default;
        $value = $request->getRequestParameter($key, $oxidDefault);
        return is_float($value) ? $value : $default;
    }

    public function getTrwIntegerRequestData(string $key, int $default = 0): int
    {
        $request = Registry::getRequest();
        /** @var string $oxidDefault */
        $oxidDefault = $default;
        $value = $request->getRequestParameter($key, $oxidDefault);
        return is_int($value) ? $value : $default;
    }

    public function getTrwArrayRequestData(string $key, array $default = []): array
    {
        $request = Registry::getRequest();
        /** @var string $oxidDefault */
        $oxidDefault = $default;
        $value = $request->getRequestParameter($key, $oxidDefault);
        return is_array($value) ? $value : $default;
    }

    public function getTrwBoolRequestData(string $key): bool
    {
        $request = Registry::getRequest();
        $value = $request->getRequestParameter($key);
        return (bool)$value;
    }
}
